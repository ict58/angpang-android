package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 21/05/2016.
 */
public class CommentPostResult {

    @SerializedName("comment_id")
    private int _commentId;

    @SerializedName("result")
    private boolean _isSuccess;

    public boolean isSuccess() {
        return _isSuccess;
    }

    public int getCommentId() {
        return _commentId;
    }
}
