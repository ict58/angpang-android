package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 21/04/2016.
 */
public class PostImageItem extends BasePostItem {

    private String _imgUrl;

    public PostImageItem(long id, int postId, int fromId, String username, String avaUrl, String role, String imgUrl,
                         String text, boolean isLiked, int totalLike, String createAt) {
        super(id, postId, fromId, username, avaUrl, role, text, isLiked, totalLike, createAt);
        _imgUrl = imgUrl;
    }

    @Override
    protected PostType setPostType() {
        return PostType.IMAGE;
    }

    public String getImageUrl() {
        return _imgUrl;
    }
}
