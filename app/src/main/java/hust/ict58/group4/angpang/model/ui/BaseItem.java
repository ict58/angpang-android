package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 20/04/2016.
 */
public abstract class BaseItem {

    private long _id;

    public BaseItem(long id) {
        _id = id;
    }

    public long getId() {
        return _id;
    }
}
