package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.model.ui.ChildNavItem;

/**
 * Created by tuanminh on 26/03/2016.
 */
public class RegisterChildListAdapter extends BaseListAdapter {


    public RegisterChildListAdapter(Context context) {
        super(context);
    }

    public void applyValueToItemList(ListView listView) {
        for (int i = 0; i < getCount(); i++) {
            View itemView = listView.getChildAt(i);

            Spinner spinner = (Spinner) itemView.findViewById(R.id.list_item_spinner_user_role);
            EditText token = (EditText) itemView.findViewById(R.id.list_item_float_edit_child_token);

            ChildNavItem child = (ChildNavItem) getItem(i);
            child.setToken(token.getText().toString());
            child.setUserRoleText(spinner.getSelectedItem().toString());
        }
    }

    @Override
    public View getViewData(final int position, View convertView, ViewGroup parent) {
        ChildNavItem child = (ChildNavItem) getItem(position);
        final ViewHolder holder;

        if (convertView == null) {
            convertView = getInflater().inflate(R.layout.list_item_add_child, parent, false);

            holder = new ViewHolder();
            holder.spinner = (Spinner) convertView.findViewById(R.id.list_item_spinner_user_role);
            holder.token = (EditText) convertView.findViewById(R.id
                    .list_item_float_edit_child_token);
            holder.removeIcon = (TextView) convertView.findViewById(R.id.list_item_register_remove_child);

            holder.removeIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeItem(position);
                }
            });

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                    R.array.array_role, R.layout.spinner_register_role);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spinner.setAdapter(adapter);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.token.setText(child.getToken());

        // Spinner
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.spinner.setSelection(child.getUserRole().ordinal());

        return convertView;
    }

    private static class ViewHolder {
        public Spinner spinner;
        public EditText token;
        public TextView removeIcon;
    }
}
