package hust.ict58.group4.angpang.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.BulletinAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.gcm.GCMPushReceiverService;
import hust.ict58.group4.angpang.model.ui.BulletinItem;

public class BulletinBoardActivity extends AppCompatActivity {

    private BroadcastReceiver _receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin_board);

        Toolbar toolbar = (Toolbar) findViewById(R.id.act_bulletin_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Bulletin Board");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        final TextView noNoti = (TextView) findViewById(R.id.act_bulletin_no_noti);

        ListView listView = (ListView) findViewById(R.id.list_view);
        final BulletinAdapter adapter = new BulletinAdapter(this);
        listView.setAdapter(adapter);

        ArrayList<BulletinItem> bulletinItems = AppController.getInstance().getBulletinItems();

        for (BulletinItem bulletinItem : bulletinItems)
            adapter.addItem(bulletinItem);

        _receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String title = intent.getStringExtra("title");
                String message = intent.getStringExtra("message");
                noNoti.setVisibility(View.GONE);
                adapter.addItem(new BulletinItem(adapter.getCount(), title, message));
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(_receiver, new IntentFilter(GCMPushReceiverService.NOTIFICATION_RECEIVED));

        if (adapter.getCount() == 0)
            noNoti.setVisibility(View.VISIBLE);
        else
            noNoti.setVisibility(View.GONE);
    }
}
