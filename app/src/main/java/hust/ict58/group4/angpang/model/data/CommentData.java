package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhhung on 5/20/16.
 */
public class CommentData {

    @SerializedName("comment_id")
    private int _commentId;

    @SerializedName("from_id")
    private int _fromId;

    @SerializedName("post_id")
    private int _postId;

    @SerializedName("comments")
    private String _commentContent;

    @SerializedName("from_user")
    private String _fromUsername;

    @SerializedName("from_ava")
    private String _fromAvatar;

    @SerializedName("created_at")
    private String _createdAt;

    public CommentData(int commentId, int fromId, int postId, String commentContent, String fromUsername)
    {
        _commentContent = commentContent;
        _commentId = commentId;
        _fromId = fromId;
        _postId = postId;
        _commentContent = commentContent;
        _fromUsername =fromUsername;
    }

    public int getCommentId()
    {
        return _commentId;
    }

    public int getFromId()
    {
        return _fromId;
    }

    public int getPostId()
    {
        return _postId;
    }

    public String getCommentContent()
    {
        return _commentContent;
    }

    public String getFromUsername()
    {
        return _fromUsername;
    }

    public String getFromAvatar()
    {
        return _fromAvatar;
    }

    public String getCreatedTime()
    {
        return _createdAt;
    }
}
