package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 21/05/2016.
 */
public class CommentPostRequest {

    @SerializedName("from_id")
    private int _fromId;

    @SerializedName("post_id")
    private int _postId;

    @SerializedName("comments")
    private String _comment;

    public CommentPostRequest(int fromId, int postId, String comment) {
        _fromId = fromId;
        _postId = postId;
        _comment = comment;
    }
}
