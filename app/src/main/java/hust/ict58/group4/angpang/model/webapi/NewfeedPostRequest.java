package hust.ict58.group4.angpang.model.webapi;

/**
 * Created by tuanminh on 20/05/2016.
 */
public class NewfeedPostRequest {

    private int class_id;

    private int child_id;

    private int from_id;

    private String post_content;

    private String image;

    public NewfeedPostRequest(int classId, int childId, int fromId, String postContent, String imageBase64) {
        class_id = classId;
        child_id = childId;
        from_id = fromId;
        post_content = postContent;
        image = imageBase64;
    }
}
