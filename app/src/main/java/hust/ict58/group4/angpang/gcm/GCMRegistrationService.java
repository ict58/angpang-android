package hust.ict58.group4.angpang.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tuanminh on 19/05/2016.
 */
public class GCMRegistrationService extends IntentService {
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    private static String token;

    public GCMRegistrationService() {
        super("");
    }

    private AppService _appService;

    @Override
    protected void onHandleIntent(Intent intent) {
        registerGCM();
    }

    private void registerGCM() {
        //Registration complete intent initially null
        Intent registrationComplete = null;

        //Register token is also null
        //we will get the token on successfull registration
        token = null;
        try {
            //Creating an instanceid
            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());

            //Getting the token from the instance id
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            //Displaying the token in the log so that we can copy it to send push notification
            //You can also extend the app by storing the token in to your server
            Log.d("GCMRegIntentService", "token:" + token);

            sendGcmIdToServer();

            //on registration complete creating intent with succes  s
            registrationComplete = new Intent(REGISTRATION_SUCCESS);

            //Putting the token to the intent
            registrationComplete.putExtra("token", token);
        } catch (Exception e) {
            //If any error occurred
            Log.d("GCMRegIntentService", "Registration error");
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }

        //Sending the broadcast that registration is completed
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendGcmIdToServer() {
        AppController appController = AppController.getInstance();
        _appService = appController.getAppService();

        Call<String> sendGcmCall = _appService.saveGcmId(appController.getCurrentUser().getID(), token);
        sendGcmCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                String result = response.body();

                if (result.compareTo("true") == 0) {
                    Log.d("SEND GCM TO SERVER", "SUCCEED");
                } else
                    Log.d("SEND GCM TO SERVER", "FAILED");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}
