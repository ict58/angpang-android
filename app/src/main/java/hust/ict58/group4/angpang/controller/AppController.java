package hust.ict58.group4.angpang.controller;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.ArrayList;

import hust.ict58.group4.angpang.gcm.GCMPushReceiverService;
import hust.ict58.group4.angpang.gcm.GCMRegistrationService;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.BulletinItem;
import hust.ict58.group4.angpang.utils.SharedPreferencesUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tuanminh on 19/04/2016.
 */
public class AppController extends Application {

    private static AppController mInstance;
    private static final String BASE_URL = "http://tuanminh.16mb.com/";

    private AppService _appService;
    private UserData _currentUser;
    private ArrayList<BulletinItem> _bulletinItems;
    private BroadcastReceiver _receiver;
    private BroadcastReceiver _gcmReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        _appService = retrofit.create(AppService.class);

        _bulletinItems = new ArrayList<>();

        _receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String title = intent.getStringExtra("title");
                String message = intent.getStringExtra("message");

                _bulletinItems.add(new BulletinItem(_bulletinItems.size(), title, message));
            }
        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(SharedPreferencesUtils.PREFS_NAME, Context.MODE_PRIVATE);
        if (pref.getBoolean(SharedPreferencesUtils.PREF_SETTING_RECEIVE_NOTI, true))
            setupGCM();

        LocalBroadcastManager.getInstance(this).registerReceiver(_receiver, new IntentFilter(GCMPushReceiverService.NOTIFICATION_RECEIVED));
    }

    private void setupGCM() {
        _gcmReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //If the broadcast has received with success
                //that means device is registered successfully
                if (intent.getAction().equals(GCMRegistrationService.REGISTRATION_SUCCESS)) {
                    //Getting the registration token from the intent
                    String token = intent.getStringExtra("token");
                    //Displaying the token as toast
                    Toast.makeText(getApplicationContext(), "Registration token:" + token, Toast.LENGTH_LONG).show();

                    //if the intent is not with success then displaying error messages
                } else if (intent.getAction().equals(GCMRegistrationService.REGISTRATION_ERROR)) {
                    Toast.makeText(getApplicationContext(), "GCM registration error!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Error occurred", Toast.LENGTH_LONG).show();
                }
            }
        };

        //Checking play service is available or not
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        //if play service is not available
        if (ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //Displaying message that play service is not installed
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                //If play service is not supported
                //Displaying an error message
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {
            //Starting intent to register device
            Intent intent = new Intent(this, GCMRegistrationService.class);
            startService(intent);
        }

        registerGCM();
    }

    public void unregisterGCM() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(_gcmReceiver);
    }

    public void registerGCM() {
        LocalBroadcastManager.getInstance(this).registerReceiver(_gcmReceiver,
                new IntentFilter(GCMRegistrationService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(_gcmReceiver,
                new IntentFilter(GCMRegistrationService.REGISTRATION_ERROR));
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public AppService getAppService() {
        return _appService;
    }

    public UserData createUser(int id, String username, String email, String avatarUrl) {
        _currentUser = new UserData(id, username, email, avatarUrl);
        return _currentUser;
    }

    public UserData getCurrentUser() {
        return _currentUser;
    }

    public ArrayList<BulletinItem> getBulletinItems() {
        return _bulletinItems;
    }
}
