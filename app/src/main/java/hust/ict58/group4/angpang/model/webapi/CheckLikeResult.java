package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 21/05/2016.
 */
public class CheckLikeResult {

    private String result;

    @SerializedName("total_like")
    private int _totalLike;

    public int getTotalLike() {
        return _totalLike;
    }

    public boolean isLike() {
        return result.compareTo("liked") == 0;
    }
}
