package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.model.ui.BulletinItem;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class BulletinAdapter extends BaseListAdapter {
    public BulletinAdapter(Context context) {
        super(context);
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = getInflater().inflate(R.layout.list_item_bulletin, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.title = (TextView) convertView.findViewById(R.id.list_item_bulletin_title);
            viewHolder.message = (TextView) convertView.findViewById(R.id.list_item_bulletin_message);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BulletinItem bulletinItem = (BulletinItem) getItem(position);

        viewHolder.title.setText(bulletinItem.getTitle());
        viewHolder.message.setText(bulletinItem.getMessage());

        return convertView;
    }

    private static class ViewHolder {
        public TextView title;
        public TextView message;
    }
}
