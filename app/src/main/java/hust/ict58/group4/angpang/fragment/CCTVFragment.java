package hust.ict58.group4.angpang.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.CCTVAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.CameraData;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.ui.Camera;
import hust.ict58.group4.angpang.utils.AnimationFactory;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CCTVFragment extends Fragment {

    private AppService _appService;
    private ChildData _currentChild;
    private CCTVAdapter _cctvAdapter;
    private View view;

    public static CCTVFragment newInstance() {
        CCTVFragment fragment = new CCTVFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            if (container != null)
                container.removeView(view);
            return view;
        }

        _appService = AppController.getInstance().getAppService();
        _currentChild = AppController.getInstance().getCurrentUser().getCurrentChild();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cctv, container, false);

        ListView listView = (ListView) view.findViewById(R.id.list_view);

        _cctvAdapter = new CCTVAdapter(getContext());

        AnimationFactory.createSwingBottomListAnimation(getActivity(), listView, _cctvAdapter, 300);

        fetchCamera();

        return view;
    }

    private void fetchCamera() {
        final Call<ArrayList<CameraData>> cameraDataCall = _appService.getCameras(_currentChild.getId());
        cameraDataCall.enqueue(new Callback<ArrayList<CameraData>>() {
            @Override
            public void onResponse(Call<ArrayList<CameraData>> call, Response<ArrayList<CameraData>> response) {
                if (!response.isSuccessful()) {
                    try {
                        ToastUtils.makeTextImageToast(getActivity(), ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                ArrayList<CameraData> cameraDatas = response.body();

                for (int i = 0; i < cameraDatas.size(); i++) {
                    CameraData cameraData = cameraDatas.get(i);
                    _cctvAdapter.addItem(new Camera(i, cameraData.getDescription(), cameraData.getCameraStreammingUrl()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CameraData>> call, Throwable t) {
                ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }
}
