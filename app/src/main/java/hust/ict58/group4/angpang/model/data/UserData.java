package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import hust.ict58.group4.angpang.model.webapi.LoginResult;

/**
 * Created by tuanminh on 04/05/2016.
 */
public class UserData {

    private int _id;
    @SerializedName("username")
    private String _username;
    @SerializedName("actor_avatar")
    private String _avatarUrl;
    @SerializedName("actor_email")
    private String _email;

    private ArrayList<ChildData> _childList;

    private ChildData _currentChild;

    public UserData(int id, String username, String email, String avatarUrl) {
        _id = id;
        _username = username;
        _email = email;
        _avatarUrl = avatarUrl;
        _childList = new ArrayList<>();
    }

    public int getID() {
        return _id;
    }

    public String getUsername() {
        return _username;
    }

    public String getEmail() {
        return _email;
    }

    public String getAvatarUrl() {
        return _avatarUrl;
    }

    public void addChildData(ArrayList<LoginResult.ChildResponse> responsesChildList) {
        for (int i = 0; i < responsesChildList.size(); i++) {
            LoginResult.ChildResponse childResponse = responsesChildList.get(i);
            _childList.add(new ChildData(childResponse.getId(), childResponse.getChildName(), childResponse.getAvatar(), childResponse.getRole(), childResponse.getChildClass(), childResponse.getClassId(), childResponse.getSchoolId()));
        }

        _currentChild = _childList.get(0);
    }

    public ArrayList<ChildData> getChildList() {
        return _childList;
    }

    public void setCurrentChild(int index) {
        _currentChild = _childList.get(index);
    }

    public void setCurrentChild(ChildData child) {
        _currentChild = child;
    }

    public ChildData getCurrentChild() {
        return _currentChild;
    }
}
