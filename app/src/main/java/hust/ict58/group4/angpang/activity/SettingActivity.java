package hust.ict58.group4.angpang.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.utils.SharedPreferencesUtils;

public class SettingActivity extends AppCompatActivity {

    private AppController _appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Settings");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        _appController = AppController.getInstance();

        final SharedPreferencesUtils sharedPreferencesUtils = SharedPreferencesUtils.getInstance();
        sharedPreferencesUtils.setContext(this);

  /*      CheckBox receiveNoti = (CheckBox) findViewById(R.id.act_setting_checkbox_receive_noti);*/
        CheckBox vibration = (CheckBox) findViewById(R.id.act_setting_checkbox_vibration);
        CheckBox sound = (CheckBox) findViewById(R.id.act_setting_checkbox_sound);

        /*receiveNoti.setChecked(sharedPreferencesUtils.isEnableRecevieNoti());*/
        vibration.setChecked(sharedPreferencesUtils.isEnableVibration());
        sound.setChecked(sharedPreferencesUtils.isEnableSound());

        /*receiveNoti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sharedPreferencesUtils.checkboxReceiveNoti(isChecked);
            }
        });*/

        vibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sharedPreferencesUtils.checkboxVibration(isChecked);
            }
        });

        sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sharedPreferencesUtils.checkboxSound(isChecked);
            }
        });
    }
}
