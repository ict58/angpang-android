package hust.ict58.group4.angpang.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.CommentAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.CommentData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.ChildNavItem;
import hust.ict58.group4.angpang.model.ui.CommentItem;
import hust.ict58.group4.angpang.model.webapi.CommentPostRequest;
import hust.ict58.group4.angpang.model.webapi.CommentPostResult;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private UserData _currentUser;
    private CommentAdapter _commentAdapter;
    private AppService _appService;
    private AppController _appController;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private ListView _listView;
    private ProgressBar _spinnerProgress;
    private EditText _editText;
    private int _postId;

    private boolean _isLockInput = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        // Get post Id from NewFeedFragment
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                ToastUtils.makeTextImageToast(CommentActivity.this, ToastUtils.ToastType.ERROR, "Check your internet connection!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                return;
            } else {
                _postId = extras.getInt("postId");
            }
        } else {
            _postId = (int) savedInstanceState.getSerializable("postId");
        }

        _appController = AppController.getInstance();
        _appService = _appController.getAppService();
        _currentUser = _appController.getCurrentUser();

        _spinnerProgress = (ProgressBar) findViewById(R.id.progress_spinner);
        _spinnerProgress.setVisibility(View.GONE);

        // List view
        _listView = (ListView) findViewById(R.id.list_view);

        _commentAdapter = new CommentAdapter(this);

        _listView.setAdapter(_commentAdapter);

        TextView postBut = (TextView) (findViewById(R.id.layout_post_input).findViewById(R.id.post_comment_input_button));
        _editText = (EditText) findViewById(R.id.post_comment_input_edit_text);

        postBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_isLockInput)
                    postComment(_editText.getText().toString());
            }
        });

        // Swipe Refresh
        _swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.act_comment_layout_swipe_refresh);
        _swipeRefreshLayout.setOnRefreshListener(this);
        _swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (!_isLockInput)
                    fetchComment();
            }
        });
    }

    private void postComment(final String commentText) {
        _spinnerProgress.setVisibility(View.VISIBLE);
        _isLockInput = true;

        Call<CommentPostResult> commentPostResultCall = _appService.postComment(new CommentPostRequest(_currentUser.getID(), _postId, commentText));
        commentPostResultCall.enqueue(new Callback<CommentPostResult>() {
            @Override
            public void onResponse(Call<CommentPostResult> call, Response<CommentPostResult> response) {
                if (!response.isSuccessful()) {
                    _isLockInput = false;
                    _spinnerProgress.setVisibility(View.GONE);
                    ToastUtils.makeTextImageToast(CommentActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    return;
                }

                CommentPostResult result = response.body();

                if (!result.isSuccess()) {
                    return;
                }

                _spinnerProgress.setVisibility(View.GONE);
                _isLockInput = false;
                _editText.setText("");

                _commentAdapter.addItem(new CommentItem(_commentAdapter.getCount(), _postId, result.getCommentId(), _currentUser.getUsername(), _currentUser.getAvatarUrl(), df.format(Calendar.getInstance().getTime()), commentText, _currentUser.getCurrentChild().getRole(), _currentUser.getID()));
                _listView.setSelection(_commentAdapter.getCount() - 1);
            }

            @Override
            public void onFailure(Call<CommentPostResult> call, Throwable t) {
                _isLockInput = false;
                _spinnerProgress.setVisibility(View.GONE);
                ToastUtils.makeTextImageToast(CommentActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private void fetchComment() {
        _isLockInput = true;
        _swipeRefreshLayout.setRefreshing(true);

        Call<ArrayList<CommentData>> getCommentCall = _appService.getComments(_postId);
        getCommentCall.enqueue(new Callback<ArrayList<CommentData>>() {
            @Override
            public void onResponse(Call<ArrayList<CommentData>> call, Response<ArrayList<CommentData>> response) {
                if (!response.isSuccessful()) {
                    _swipeRefreshLayout.setRefreshing(false);
                    _isLockInput = false;
                    ToastUtils.makeTextImageToast(CommentActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
                    return;
                }

                ArrayList<CommentData> commentDatas = response.body();

                for (int i = 0; i < commentDatas.size(); i++) {
                    CommentData commentData = commentDatas.get(i);

                    _commentAdapter.addItem(new CommentItem(i, _postId, commentData.getCommentId(), commentData.getFromUsername(), commentData.getFromAvatar(), commentData.getCreatedTime(), commentData.getCommentContent(), ChildNavItem.UserRole.SUPERVISOR.toString(), commentData.getFromId()));
                }

                _swipeRefreshLayout.setRefreshing(false);
                _isLockInput = false;
            }

            @Override
            public void onFailure(Call<ArrayList<CommentData>> call, Throwable t) {
                ToastUtils.makeTextImageToast(CommentActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                _swipeRefreshLayout.setRefreshing(false);
                _isLockInput = false;
            }
        });
    }

    @Override
    public void onRefresh() {
        _commentAdapter.clearAll();
        fetchComment();
    }
}
