package hust.ict58.group4.angpang.model.data;

import java.util.ArrayList;

import hust.ict58.group4.angpang.model.ui.ContactItem;

/**
 * Created by tuanminh on 04/05/2016.
 */
public class ChildData extends BaseChildData {

    private int _classId;
    private int _schoolId;
    private String _className;

    private ArrayList<ContactItem> _contactList;

    public ChildData(int id, String name, String avatar, String role, String className, int classId, int schoolId) {
        super(id, name, avatar, role);
        _classId = classId;
        _schoolId = schoolId;
        _className = className;
        _contactList = new ArrayList<>();
    }

    public void addContactList(ArrayList<TeacherData> teacherDatas) {
        for (int i = 0; i < teacherDatas.size(); i++) {
            TeacherData teacherData = teacherDatas.get(i);
            _contactList.add(new ContactItem(i, teacherData.getName(), teacherData.getPhoneNumber(), teacherData.getEmail(), teacherData.getAddress()));
        }
    }

    public boolean hasLoadedContactList() {
        return _contactList.size() > 0;
    }

    public ArrayList<ContactItem> getContactList() {
        return _contactList;
    }

    public int getClassId() {
        return _classId;
    }

    public int getSchoolId() {
        return _schoolId;
    }

    public String getClassName() {
        return _className;
    }
}
