package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhhung on 5/20/16.
 */
public class ImageData {

    @SerializedName("media_id")
    private int _id;

    @SerializedName("album_name")
    private String _albumName;

    @SerializedName("media_url")
    private String _url;

    @SerializedName("media_description")
    private String _description;

    public ImageData(int mediaId, String albumName, String url, String description)
    {
        _id = mediaId;
        _albumName = albumName;
        _url = url;
        _description = description;
    }

    public int getId()
    {
        return _id;
    }

    public String getAlbumName()
    {
        return _albumName;
    }

    public String getUrl()
    {
        return _url;
    }

    public String getDescription()
    {
        return _description;
    }
}
