package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import hust.ict58.group4.angpang.model.ui.BaseItem;

/**
 * Created by tuanminh on 21/04/2016.
 */
public abstract class BaseListAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<BaseItem> _itemList;
    private LayoutInflater _inflater;

    public BaseListAdapter(Context context) {
        _context = context;
        _inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        _itemList = new ArrayList<>();
    }

    public void addItem(BaseItem item) {
        _itemList.add(item);
        notifyDataSetChanged();
    }

    public void addItem(BaseItem item, int index) {
        _itemList.add(index, item);
        notifyDataSetChanged();
    }

    public void addFirst(BaseItem item) {
        _itemList.add(0, item);
        notifyDataSetChanged();
    }

    public void removeItem(int index) {
        _itemList.remove(index);
        notifyDataSetChanged();
    }

    public void removeItem(BaseItem item) {
        _itemList.remove(item);
        notifyDataSetChanged();
    }

    public void clearAll()
    {
        _itemList.clear();
        notifyDataSetChanged();
    }

    public void clearAllExceptFirst()
    {
        for(int i = getCount() - 1; i >= 1; i--)
        {
            _itemList.remove(i);
        }
        notifyDataSetChanged();
    }

    public Context getContext() {
        return _context;
    }

    public LayoutInflater getInflater() {
        return _inflater;
    }

    public ArrayList<BaseItem> getItemList() {
        return _itemList;
    }

    public int getNextId() {
        return getCount() - 1;
    }

    @Override
    public int getCount() {
        return _itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return _itemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return ((BaseItem) getItem(i)).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return getViewData(i, view, viewGroup);
    }

    public abstract View getViewData(int position, View convertView, ViewGroup parent);
}
