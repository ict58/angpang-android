package hust.ict58.group4.angpang.controller;

import java.util.ArrayList;

import hust.ict58.group4.angpang.model.data.CameraData;
import hust.ict58.group4.angpang.model.data.CommentData;
import hust.ict58.group4.angpang.model.data.FoodMenuData;
import hust.ict58.group4.angpang.model.data.ImageData;
import hust.ict58.group4.angpang.model.data.PostData;
import hust.ict58.group4.angpang.model.data.ProfileChildData;
import hust.ict58.group4.angpang.model.data.TeacherData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.webapi.CommentPostRequest;
import hust.ict58.group4.angpang.model.webapi.CommentPostResult;
import hust.ict58.group4.angpang.model.webapi.HealthStatusResponse;
import hust.ict58.group4.angpang.model.webapi.LoginRequest;
import hust.ict58.group4.angpang.model.webapi.LoginResult;
import hust.ict58.group4.angpang.model.webapi.NewfeedPostRequest;
import hust.ict58.group4.angpang.model.webapi.NewfeedPostResult;
import hust.ict58.group4.angpang.model.webapi.RegisterRequest;
import hust.ict58.group4.angpang.model.webapi.RegisterResult;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by khanhhung on 5/16/16.
 */
public interface AppService {

    @POST("phoneLogin")
    Call<LoginResult> loginUser(@Body LoginRequest loginRequestData);

    @POST("phoneRegister")
    Call<RegisterResult> register(@Body RegisterRequest registerRequest);

    @GET("getuser/{userId}")
    Call<UserData> getUser(@Path("userId") int userId);

    // SOCIAL NETWORK
    @POST("snspost")
    Call<NewfeedPostResult> postNewfeed(@Body NewfeedPostRequest newfeedPostRequest);

    @POST("snscomment")
    Call<CommentPostResult> postComment(@Body CommentPostRequest commentPostRequest);

    @GET("getchild/{userId}")
    Call<ArrayList<ProfileChildData>> getChildByUserId(@Path("userId") int userId);

    @GET("getpost/{childId}/{actorId}")
    Call<ArrayList<PostData>> getPosts(@Path("childId") int childId, @Path("actorId") int actorId);

    @GET("getcomment/{postId}")
    Call<ArrayList<CommentData>> getComments(@Path("postId") int postId);

    @GET("checklike/{actorId}/{postId}")
    Call<String> checkLike(@Path("actorId") int actorId, @Path("postId") int postId);

    // HEALTH STATUS
    @GET("gethealth/{childId}")
    Call<HealthStatusResponse> getHealthStatus(@Path("childId") int childId);

    //TimeTable
    @GET("getfood/{classId}")
    Call<ArrayList<FoodMenuData>> getFoodMenu(@Path("classId") int classId);

    //CCTV
    @GET("getcctv/{classId}")
    Call<ArrayList<CameraData>> getCameras(@Path("classId") int classId);

    //Album
    @GET("getphoto/{classId}")
    Call<ArrayList<ImageData>> getAlbumImages(@Path("classId") int childId);

    @GET("getteacher/{classId}")
    Call<ArrayList<TeacherData>> getTeacherData(@Path("classId") int classId);

    @GET("savegcmid/{actorId}/{gcmId}")
    Call<String> saveGcmId(@Path("actorId") int actorId, @Path("gcmId") String gcmId);
}
