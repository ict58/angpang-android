package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhhung on 5/16/16.
 */
public class HealthStatusResponse {

    @SerializedName("health")
    private String overallHealth;

    @SerializedName("activity")
    private String currentActivity;

    @SerializedName("mood")
    private String mood;

    @SerializedName("weight")
    private String weight;

    @SerializedName("height")
    private String height;

    public String getOverallHealth() {
        return overallHealth;
    }

    public String getCurrentActivity() {
        return currentActivity;
    }

    public String getMood() {
        return mood;
    }

    public String getWeight() {
        return weight;
    }

    public String getHeight() {
        return height;
    }
}
