package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.model.ui.ContactItem;

/**
 * Created by tuanminh on 04/05/2016.
 */
public class ContactAdapter extends BaseListAdapter {
    public ContactAdapter(Context context) {
        super(context);
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        ContactViewHolder contactViewHolder;

        if (convertView == null) {

            convertView = getInflater().inflate(R.layout.list_item_contact, parent, false);
            contactViewHolder = new ContactViewHolder();

            contactViewHolder.name = (TextView) convertView.findViewById(R.id.list_item_contact_name);
            contactViewHolder.phoneNumber = (TextView) convertView.findViewById(R.id.list_item_contact_text_phone);
            contactViewHolder.email = (TextView) convertView.findViewById(R.id.list_item_contact_text_email);
            contactViewHolder.address = (TextView) convertView.findViewById(R.id.list_item_contact_text_address);

            convertView.setTag(contactViewHolder);
        } else {
            contactViewHolder = (ContactViewHolder) convertView.getTag();
        }

        ContactItem contactItem = (ContactItem) getItem(position);

        contactViewHolder.name.setText(contactItem.getName());
        contactViewHolder.phoneNumber.setText(contactItem.getPhone());
        contactViewHolder.email.setText(contactItem.getEmail());
        contactViewHolder.address.setText(contactItem.getAddress());

        return convertView;
    }

    private static class ContactViewHolder {
        private TextView name;
        private TextView phoneNumber;
        private TextView email;
        private TextView address;
    }
}
