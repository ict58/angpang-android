package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by tuanminh on 24/04/2016.
 */
public class LoginResult {

    private boolean result;

    @SerializedName("actor_id")
    private int userId;

    @SerializedName("actor_avatar")
    private String avatarUrl;

    @SerializedName("actor_email")
    private String userEmail;

    @SerializedName("child_list")
    private ArrayList<ChildResponse> childResponseArrayList;

    public boolean isLoginSuccess() {
        return result;
    }

    public int getUserId() {
        return userId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public ArrayList<ChildResponse> getChildResponseArrayList() {
        return childResponseArrayList;
    }

    public class ChildResponse {
        @SerializedName("child_name")
        private String childName;

        @SerializedName("child_id")
        private int childId;

        @SerializedName("role")
        private String role;

        @SerializedName("class_id")
        private int classId;

        @SerializedName("child_avatar")
        private String avatar;

        @SerializedName("class_name")
        private String className;

        @SerializedName("school_id")
        private int _schoolId;

        public int getId() {
            return childId;
        }

        public String getChildName() {
            return childName;
        }

        public String getRole() {
            return "";
        }

        public String getAvatar() {
            return avatar;
        }

        public String getChildClass() {
            return className;
        }

        public int getClassId() {
            return classId;
        }

        public int getSchoolId() {
            return _schoolId;
        }
    }
}
