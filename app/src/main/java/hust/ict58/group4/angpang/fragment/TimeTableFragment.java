package hust.ict58.group4.angpang.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.data.FoodMenuData;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimeTableFragment extends Fragment implements WeekView.EventClickListener, MonthLoader.MonthChangeListener {

    private AppService _appService;
    private ChildData _currentChild;
    private View view;
    private WeekView _weekView;

    private AlertDialog _foodMenuDialog;
    private ArrayList<FoodMenuData> _foodMenuDatas;

    public TimeTableFragment() {
        // Required empty public constructor
    }

    public static TimeTableFragment newInstance() {
        TimeTableFragment fragment = new TimeTableFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            if (container != null)
                container.removeView(view);

            _currentChild = AppController.getInstance().getCurrentUser().getCurrentChild();
            return view;
        }

        view = inflater.inflate(R.layout.fragment_time_table, container, false);

        _appService = AppController.getInstance().getAppService();
        _currentChild = AppController.getInstance().getCurrentUser().getCurrentChild();

        _weekView = (WeekView) view.findViewById(R.id.timetable_week_view);
        _weekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        _weekView.setMonthChangeListener(this);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        _foodMenuDialog = new AlertDialog.Builder(getContext()).create();

        fetchFoodMenu();

        return view;
    }

    private void showFoodDialog(String date, String foodMenu) {
        _foodMenuDialog.setTitle("Lunch Menu " + date);
        _foodMenuDialog.setMessage(foodMenu);
        _foodMenuDialog.show();
    }

    private void setupDateTimeInterpreter(final boolean shortDate) {
        _weekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
            }
        });
    }

    protected String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        if (_foodMenuDialog == null || _foodMenuDatas == null)
            return;

        int calendarDay = (int) event.getId();
        FoodMenuData foodMenu;
        try {
            foodMenu = getFoodMenuByDay(calendarDay);
        } catch (NullPointerException e) {
            ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, "No food menu data for this day!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            return;
        }
        showFoodDialog(foodMenu.getDate(), foodMenu.getFoodMenu());
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> events = new ArrayList<>();

        for (int i = 1; i <= 7; i++) {
            Calendar startTime = Calendar.getInstance();
            startTime.set(Calendar.HOUR_OF_DAY, 11);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.MONTH, newMonth - 1);
            startTime.set(Calendar.YEAR, newYear);
            startTime.set(Calendar.DAY_OF_WEEK, i + 1);
            Calendar endTime = (Calendar) startTime.clone();
            endTime.add(Calendar.HOUR, 1);
            endTime.set(Calendar.MONTH, newMonth - 1);
            endTime.set(Calendar.DAY_OF_WEEK, i + 1);
            WeekViewEvent event = new WeekViewEvent(i, "Lunch", startTime, endTime);
            event.setColor(getResources().getColor(R.color.event_color_01));
            events.add(event);
        }

        return events;
    }

    private void fetchFoodMenu() {
        Call<ArrayList<FoodMenuData>> foodMenuCall = _appService.getFoodMenu(_currentChild.getClassId());
        foodMenuCall.enqueue(new Callback<ArrayList<FoodMenuData>>() {
            @Override
            public void onResponse(Call<ArrayList<FoodMenuData>> call, Response<ArrayList<FoodMenuData>> response) {
                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    return;
                }

                _foodMenuDatas = response.body();
            }

            @Override
            public void onFailure(Call<ArrayList<FoodMenuData>> call, Throwable t) {
                ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private FoodMenuData getFoodMenuByDay(int calendarDay) {
        for (int i = 0; i < _foodMenuDatas.size(); i++) {
            FoodMenuData foodMenuData = _foodMenuDatas.get(i);
            if (foodMenuData.compareToCalendarDay(calendarDay)) {
                return foodMenuData;
            }
        }

        return null;
    }
}
