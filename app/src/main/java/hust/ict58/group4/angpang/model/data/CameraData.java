package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 17/05/2016.
 */
public class CameraData {

    @SerializedName("camera_id")
    private int _cameraId;

    @SerializedName("cctv_description")
    private String _description;

    @SerializedName("cctv_ip")
    private String _ip;

    @SerializedName("class_id")
    private int _classId;

    public int getCameraId() {
        return _cameraId;
    }

    public String getDescription() {
        return _description;
    }

    public String getCameraStreammingUrl() {
        return _ip;
    }

    public int getClassId() {
        return _classId;
    }
}
