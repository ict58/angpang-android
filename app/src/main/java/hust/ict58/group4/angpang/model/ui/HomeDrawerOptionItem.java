package hust.ict58.group4.angpang.model.ui;


import hust.ict58.group4.angpang.adapter.HomeDrawerAdapter;

/**
 * Created by tuanminh on 27/03/2016.
 */
public class HomeDrawerOptionItem extends BaseHomeDrawerItem {

    private String _icon;
    private String _optionName;

    public HomeDrawerOptionItem(long id, String iconRes, String optionName, Runnable callback) {
        super(id, callback);
        _icon = iconRes;
        _optionName = optionName;
    }

    @Override
    public HomeDrawerAdapter.ViewType setViewType() {
        return HomeDrawerAdapter.ViewType.OPTION_LAYOUT;
    }

    public String getIcon() {
        return _icon;
    }

    public String getOptionName() {
        return _optionName;
    }
}
