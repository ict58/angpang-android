package hust.ict58.group4.angpang.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.SwipeDismissAdapter;

import java.io.IOException;
import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.NewFeedPostAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.PostData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.PostImageItem;
import hust.ict58.group4.angpang.model.ui.PostTextItem;
import hust.ict58.group4.angpang.utils.AnimationFactory;
import hust.ict58.group4.angpang.utils.ImageUtils;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewFeedFragment extends Fragment implements OnDismissCallback, SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private AppService _appService;
    private AppController _appController;
    private ProgressBar _spinnerProgress;

    public NewFeedFragment() {
        // Required empty public constructor
    }


    public static NewFeedFragment newInstance() {
        NewFeedFragment fragment = new NewFeedFragment();
        return fragment;
    }

    private NewFeedPostAdapter _newFeedPostAdapter;
    private ListView _listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            if (container != null)
                container.removeView(view);
            return view;
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_newfeed, container, false);

        _spinnerProgress = (ProgressBar) view.findViewById(R.id.progress_spinner);
        _spinnerProgress.setVisibility(View.GONE);

        _appController = AppController.getInstance();
        _appService = _appController.getAppService();

        _listView = (ListView) view.findViewById(R.id.list_view);

        _swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.frag_newfeed_layout_swipe_refresh);
        _swipeRefreshLayout.setOnRefreshListener(this);
        _swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                fetchPost();
            }
        });

        _newFeedPostAdapter = new NewFeedPostAdapter(getContext(), _spinnerProgress);
        _newFeedPostAdapter.setFragment(this);

        AnimationFactory.createSwingBottomListAnimation(getActivity(), _listView, new SwipeDismissAdapter(_newFeedPostAdapter, this), 300);

        return view;
    }

    private void fetchPost() {
        _swipeRefreshLayout.setRefreshing(true);

        UserData currentUser = _appController.getCurrentUser();

        Call<ArrayList<PostData>> postDataCall = _appService.getPosts(currentUser.getCurrentChild().getId(), currentUser.getID());
        postDataCall.enqueue(new Callback<ArrayList<PostData>>() {
            @Override
            public void onResponse(Call<ArrayList<PostData>> call, Response<ArrayList<PostData>> response) {
                if (!response.isSuccessful()) {
                    _swipeRefreshLayout.setRefreshing(false);
                    try {
                        ToastUtils.makeTextImageToast(getActivity(), ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                ArrayList<PostData> postDatas = response.body();

                for (int i = 0; i < postDatas.size(); i++) {
                    PostData postData = postDatas.get(i);

                    if (postData.getImageAttachment().compareTo("101") == 0) {
                        PostTextItem textPost = new PostTextItem(i, postData.getPostId(), postData.getFromId(), postData.getFromUsername(), postData.getFromAvatar(), "", postData.getPostContent(), postData.isLiked(), postData.getTotalLike(), postData.getCreatedTime());
                        _newFeedPostAdapter.addItem(textPost);
                    } else {
                        PostImageItem imagePost = new PostImageItem(i, postData.getPostId(), postData.getFromId(), postData.getFromUsername(), postData.getFromAvatar(), "", postData.getImageAttachment(), postData.getPostContent(), postData.isLiked(), postData.getTotalLike(), postData.getCreatedTime());
                        _newFeedPostAdapter.addItem(imagePost);
                    }
                }

                _swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<PostData>> call, Throwable t) {
                _swipeRefreshLayout.setRefreshing(false);
                ToastUtils.makeTextImageToast(getActivity(), ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }


    @Override
    public void onDismiss(@NonNull ViewGroup listView, @NonNull int[] reverseSortedPositions) {
        int index;
        for (int i = 0; i < reverseSortedPositions.length; i++) {
            index = reverseSortedPositions[i];
            _newFeedPostAdapter.removeItem(index);

            if (index == 0) {
                _newFeedPostAdapter.createInput();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        View inputPostView = _listView.getChildAt(0);
        ImageView imageView = (ImageView) inputPostView.findViewById(R.id.post_input_image);
        ImageUtils.loadImageGallery(getActivity(), imageView, requestCode, resultCode, data);

        if (imageView.getDrawable() != null)
            imageView.setVisibility(View.VISIBLE);
        else
            imageView.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        _newFeedPostAdapter.clearAllExceptFirst();
        fetchPost();
    }
}
