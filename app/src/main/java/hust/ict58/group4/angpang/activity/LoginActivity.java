package hust.ict58.group4.angpang.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.webapi.LoginRequest;
import hust.ict58.group4.angpang.model.webapi.LoginResult;
import hust.ict58.group4.angpang.utils.SharedPreferencesUtils;
import hust.ict58.group4.angpang.utils.ToastUtils;
import hust.ict58.group4.angpang.view.FloatLabeledEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private ProgressDialog _dialog;
    private AppService _loginService;
    private CheckBox _rememberMe;
    private SharedPreferencesUtils _prefUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        _prefUtils = SharedPreferencesUtils.getInstance();
        _prefUtils.setContext(this);

        _dialog = new ProgressDialog(this);
        _dialog.setMessage("Connecting to server");

        _loginService = AppController.getInstance().getAppService();

        TextView loginBut = (TextView) findViewById(R.id.but_login);
        final FloatLabeledEditText username = (FloatLabeledEditText) findViewById(R.id.act_login_edit_username);
        final FloatLabeledEditText password = (FloatLabeledEditText) findViewById(R.id.act_login_edit_password);

        _rememberMe = (CheckBox) findViewById(R.id.act_login_check_remember_me);

        if (_prefUtils.isRememberMeChecked()) {
            username.setText(_prefUtils.getRememberedUsername());
            password.setText(_prefUtils.getRememberedPassword());
            _rememberMe.toggle();
        }

        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryLogin(username.getTextString(), password.getTextString());
            }
        });

        TextView signBut = (TextView) findViewById(R.id.but_sign_up);

        signBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void tryLogin(final String username, final String password) {
        _dialog.show();

        Call<LoginResult> callLogin = _loginService.loginUser(new LoginRequest(username, password));
        callLogin.enqueue(new Callback<LoginResult>() {
            @Override
            public void onResponse(Call<LoginResult> call, Response<LoginResult> response) {

                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast(LoginActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    _dialog.hide();
                    return;
                }

                LoginResult loginResult = response.body();

                if (loginResult.isLoginSuccess()) {

                    UserData userData = AppController.getInstance().createUser(loginResult.getUserId(), username, loginResult.getUserEmail(), loginResult.getAvatarUrl());

                    userData.addChildData(loginResult.getChildResponseArrayList());

                    if (_rememberMe.isChecked()) {
                        _prefUtils.rememberAccount(username, password);
                    } else {
                        _prefUtils.uncheckRememberMe();
                    }

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    ToastUtils.makeTextImageToast(LoginActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.login_wrong_username_password), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    _dialog.hide();
                }
            }

            @Override
            public void onFailure(Call<LoginResult> call, Throwable t) {
                _dialog.hide();
                ToastUtils.makeTextImageToast(LoginActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onStop() {
        super.onStop();

        if (_dialog != null)
            _dialog.hide();
    }
}
