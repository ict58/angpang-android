package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 27/04/2016.
 */
public class AlbumImageItem extends BaseImage {

    private String _desc;

    public AlbumImageItem(long id, String url, String description) {
        super(id, url);
        _desc = description;
    }

    public String getDescription() {
        return _desc;
    }
}
