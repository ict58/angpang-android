package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 01/05/2016.
 */
public class RegisterResult {

    @SerializedName("result")
    private String result[];

    public boolean isRegisterSuccess(int i) {
        if (result[i].compareTo("success") == 0)
            return true;
        else
            return false;
    }

    public String getResult(int i) {
        return result[i];
    }

    public int getResultLength() {
        return result.length;
    }
}
