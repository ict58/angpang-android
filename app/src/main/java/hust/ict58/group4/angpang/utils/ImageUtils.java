package hust.ict58.group4.angpang.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by tuanminh on 29/03/2016.
 */
public class ImageUtils {
    private static final int PICK_IMAGE_REQUEST = 1;

    public static void loadImageUrl(Context context, ImageView imageView, String url) {
        if (url.compareTo("") == 0)
            return;

        try {
            Picasso.with(context).load(url).into(imageView);
        } catch (NullPointerException e) {
            ToastUtils.makeTextImageToast((Activity) context, ToastUtils.ToastType.ERROR, e.getMessage(), Gravity.CENTER_HORIZONTAL, Toast.LENGTH_SHORT);
        }
    }

    public static void chooseImageFromGallery(Fragment fragment) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }

    public static void loadImageGallery(Activity context, ImageView imageView, int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String encodeToBase64(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();

        if (drawable == null)
            return "";

        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        return encodedImage;
    }

}
