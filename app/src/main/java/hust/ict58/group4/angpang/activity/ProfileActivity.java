package hust.ict58.group4.angpang.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.ProfileAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.ProfileChildData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.ChildProfileItem;
import hust.ict58.group4.angpang.utils.ImageUtils;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private UserData _ownerUser;
    private AppService _appService;
    private int _userId = -1;

    private ArrayList<ProfileChildData> _childProfiles;
    private ProfileAdapter _profileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        _ownerUser = null;

        // Get post Id from NewFeedFragment
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                ToastUtils.makeTextImageToast(ProfileActivity.this, ToastUtils.ToastType.ERROR, "Check your internet connection!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                return;
            } else {
                _userId = extras.getInt("userId");
            }
        } else {
            _userId = (int) savedInstanceState.getSerializable("userId");
        }

        AppController appController = AppController.getInstance();

        _appService = appController.getAppService();

        Call<UserData> userDataCall = _appService.getUser(_userId);
        userDataCall.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                if (!response.isSuccessful()) {
                    try {
                        ToastUtils.makeTextImageToast(ProfileActivity.this, ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                _ownerUser = response.body();
                createUI();
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                ToastUtils.makeTextImageToast(ProfileActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private void createUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.act_profile_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Profile");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ImageView avatarImageView = (ImageView) findViewById(R.id.toolbar_tab_social_image);
        ImageUtils.loadImageUrl(ProfileActivity.this, avatarImageView, _ownerUser.getAvatarUrl());

        TextView nameTextView = (TextView) findViewById(R.id.toolbar_tab_social_name);
        nameTextView.setText(_ownerUser.getUsername());

        TextView email = (TextView) findViewById(R.id.toolbar_tab_social_place);
        email.setText(_ownerUser.getEmail());

        _profileAdapter = new ProfileAdapter(ProfileActivity.this);
        ListView listView = (ListView) findViewById(R.id.act_profile_list_view);
        listView.setAdapter(_profileAdapter);

        fetchChildProfiles();
    }

    private void fetchChildProfiles() {
        Call<ArrayList<ProfileChildData>> childProfileCall = _appService.getChildByUserId(_userId);
        childProfileCall.enqueue(new Callback<ArrayList<ProfileChildData>>() {
            @Override
            public void onResponse(Call<ArrayList<ProfileChildData>> call, Response<ArrayList<ProfileChildData>> response) {
                if (!response.isSuccessful()) {
                    try {
                        ToastUtils.makeTextImageToast(ProfileActivity.this, ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                _childProfiles = response.body();

                for (int i = 0; i < _childProfiles.size(); i++) {
                    ProfileChildData childData = _childProfiles.get(i);
                    _profileAdapter.addItem(new ChildProfileItem(i, childData.getName(), childData.getAvatar(), childData.getGender(), childData.getBirthday(), childData.getRole()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProfileChildData>> call, Throwable t) {
                ToastUtils.makeTextImageToast(ProfileActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }
}
