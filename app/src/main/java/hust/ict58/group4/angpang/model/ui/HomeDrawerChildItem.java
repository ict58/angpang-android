package hust.ict58.group4.angpang.model.ui;

import hust.ict58.group4.angpang.adapter.HomeDrawerAdapter;

/**
 * Created by tuanminh on 27/03/2016.
 */
public class HomeDrawerChildItem extends BaseHomeDrawerItem {

    private String _avatarUrl;
    private String _name;

    public HomeDrawerChildItem(long id, String avatarUrl, String name, Runnable callback) {
        super(id, callback);
        _avatarUrl = avatarUrl;
        _name = name;
    }

    @Override
    public HomeDrawerAdapter.ViewType setViewType() {
        return HomeDrawerAdapter.ViewType.CHILD_LAYOUT;
    }

    public String getAvatarUrl() {
        return _avatarUrl;
    }

    public String getChildName() {
        return _name;
    }
}
