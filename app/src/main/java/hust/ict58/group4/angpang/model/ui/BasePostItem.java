package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 20/04/2016.
 */
public abstract class BasePostItem extends BaseItem {

    public enum PostType {TEXT, IMAGE, INPUT}

    private int _fromId;
    private int _postId;
    private String _username;
    private String _avaUrl;
    private String _role;
    private String _text;
    private boolean _isLiked;
    private int _totalLike;
    private String _createAt;
    private PostType _postType;

    public BasePostItem(long id, int postId, int fromId, String username, String avaUrl, String role, String
            text, boolean isLiked, int totalLike, String createAt) {
        super(id);

        _fromId = fromId;
        _postId = postId;
        _username = username;
        _avaUrl = avaUrl;
        _role = role;
        _text = text;
        _isLiked = isLiked;
        _totalLike = totalLike;
        _createAt = createAt;
        _postType = setPostType();
    }

    public int getFromId() {
        return _fromId;
    }

    public int getPostId() {
        return _postId;
    }

    public String getUserName() {
        return _username;
    }

    public String getAvaUrl() {
        return _avaUrl;
    }

    public String getUserRole() {
        return _role;
    }

    public String getPostText() {
        return _text;
    }

    public boolean isLiked() {
        return _isLiked;
    }

    public int getToltalLike() {
        return _totalLike;
    }

    public PostType getPostType() {
        return _postType;
    }

    public String getCreatedTime() {
        return _createAt;
    }

    protected abstract PostType setPostType();
}
