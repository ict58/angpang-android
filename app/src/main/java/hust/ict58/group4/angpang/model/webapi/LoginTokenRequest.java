package hust.ict58.group4.angpang.model.webapi;

/**
 * Created by tuanminh on 24/04/2016.
 */
public class LoginTokenRequest {

    private String _token;

    public void setToken(String token)
    {
        _token = token;
    }

    public String getToken()
    {
        return _token;
    }
}
