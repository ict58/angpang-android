package hust.ict58.group4.angpang.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.RegisterChildListAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.ui.BaseItem;
import hust.ict58.group4.angpang.model.ui.ChildNavItem;
import hust.ict58.group4.angpang.model.webapi.RegisterRequest;
import hust.ict58.group4.angpang.model.webapi.RegisterResult;
import hust.ict58.group4.angpang.utils.ToastUtils;
import hust.ict58.group4.angpang.view.FloatLabeledEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends Activity {

    private AppService _registerService;
    private ProgressDialog _dialog;
    private Thread _toastThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        _registerService = AppController.getInstance().getAppService();
        _toastThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(Toast.LENGTH_LONG);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        _dialog = new ProgressDialog(this);
        _dialog.setMessage("Connecting to server");

        final ListView lv = (ListView) findViewById(R.id.act_register_list_child);

        final FloatLabeledEditText username = (FloatLabeledEditText) findViewById(R.id.act_register_username);
        final FloatLabeledEditText password = (FloatLabeledEditText) findViewById(R.id.act_register_password);
        final FloatLabeledEditText email = (FloatLabeledEditText) findViewById(R.id.act_register_email);

        final RegisterChildListAdapter adapter = new RegisterChildListAdapter(this);
        lv.setAdapter(adapter);

        // Add ChildNavItem But
        TextView addChildBut = (TextView) findViewById(R.id.act_register_but_add_child);
        addChildBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.addItem(new ChildNavItem(adapter.getNextId()));
            }
        });

        // Register But
        TextView registerBut = (TextView) findViewById(R.id.act_register_but_register);
        registerBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.applyValueToItemList(lv);
                tryRegister(username.getTextString(), password.getTextString(), email.getTextString(), adapter.getItemList());
            }
        });
    }

    private void tryRegister(String username, String password, String email, ArrayList<BaseItem> children) {
        _dialog.show();

        RegisterRequest registerRequest = new RegisterRequest(username, password, email);

        for (int i = 0; i < children.size(); i++) {
            ChildNavItem child = (ChildNavItem) children.get(i);
            registerRequest.registerChild(child.getToken(), child.getUserRole().toString());
        }

        final Call<RegisterResult> registerResultCall = _registerService.register(registerRequest);
        registerResultCall.enqueue(new Callback<RegisterResult>() {
            @Override
            public void onResponse(Call<RegisterResult> call, Response<RegisterResult> response) {
                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast(RegisterActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
                    _dialog.hide();
                    return;
                }

                RegisterResult responseBody = response.body();

                for (int i = 0; i < responseBody.getResultLength(); i++) {
                    if (response.body().isRegisterSuccess(i)) {
                        ToastUtils.makeTextImageToast(RegisterActivity.this, ToastUtils.ToastType.OK, responseBody.getResult(i), Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
                        _toastThread.start();
                    } else {
                        ToastUtils.makeTextImageToast(RegisterActivity.this, ToastUtils.ToastType.ERROR, responseBody.getResult(i), Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
                    }
                }

                _dialog.hide();
            }

            @Override
            public void onFailure(Call<RegisterResult> call, Throwable t) {
                _dialog.hide();
                ToastUtils.makeTextImageToast(RegisterActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_dialog != null)
            _dialog.hide();
    }
}
