package hust.ict58.group4.angpang.model.ui;

import android.view.View;
import android.view.View.OnClickListener;

import hust.ict58.group4.angpang.adapter.HomeDrawerAdapter;

/**
 * Created by tuanminh on 27/03/2016.
 */
public abstract class BaseHomeDrawerItem extends BaseItem implements OnClickListener {
    private HomeDrawerAdapter.ViewType _viewType;
    private Runnable _callback;

    public BaseHomeDrawerItem(long id, Runnable callback) {
        super(id);
        _viewType = setViewType();
        _callback = callback;
    }

    public abstract HomeDrawerAdapter.ViewType setViewType();

    public HomeDrawerAdapter.ViewType getViewType() {
        return _viewType;
    }

    @Override
    public void onClick(View view) {
        _callback.run();
    }
}
