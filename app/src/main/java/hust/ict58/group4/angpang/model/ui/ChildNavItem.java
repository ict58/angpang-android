package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 26/03/2016.
 */
public class ChildNavItem extends BaseItem {

    public enum UserRole {DAD, MOM, SUPERVISOR}

    private String token;
    private String userRoleText;
    private UserRole currentUserRole;

    public ChildNavItem(long id) {
        super(id);
        token = "";
        currentUserRole = UserRole.DAD;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setUserRole(UserRole role) {
        currentUserRole = role;
    }

    public UserRole getUserRole() {
        return currentUserRole;
    }

    public void setUserRoleText(String role) {
        userRoleText = role;
    }

    public String getUserRoleText() {
        return userRoleText;
    }
}
