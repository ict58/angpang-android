package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 27/04/2016.
 */
public class AvatarImage extends BaseImage {
    public AvatarImage(long id, String url) {
        super(id, url);
    }
}
