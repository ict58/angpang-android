package hust.ict58.group4.angpang.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.activity.CommentActivity;
import hust.ict58.group4.angpang.activity.ProfileActivity;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.fragment.NewFeedFragment;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.BasePostItem;
import hust.ict58.group4.angpang.model.ui.PostImageItem;
import hust.ict58.group4.angpang.model.ui.PostInputItem;
import hust.ict58.group4.angpang.model.ui.PostTextItem;
import hust.ict58.group4.angpang.model.webapi.NewfeedPostRequest;
import hust.ict58.group4.angpang.model.webapi.NewfeedPostResult;
import hust.ict58.group4.angpang.utils.ImageUtils;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tuanminh on 21/04/2016.
 */
public class NewFeedPostAdapter extends BaseListAdapter {

    private AppService _appService;
    private UserData _currentUser;
    private boolean _isLockInput = false;
    private ProgressBar _spinnerProgress;

    public NewFeedPostAdapter(Context context, ProgressBar spinner) {
        super(context);
        _currentUser = AppController.getInstance().getCurrentUser();
        _appService = AppController.getInstance().getAppService();
        _spinnerProgress = spinner;

        createInput();
    }

    private NewFeedFragment _fragment;

    public void setFragment(NewFeedFragment fragment) {
        _fragment = fragment;
    }

    public void createInput() {
        addFirst(new PostInputItem(-1, -1, -1, "", "", null, "", false, 0, ""));
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        BasePostItem.PostType postType = ((BasePostItem) getItem(position)).getPostType();

        switch (postType) {
            case INPUT:
                final InputPostViewHolder inputPostHolder;

                if (convertView == null) {
                    convertView = getInflater().inflate(R.layout.post_input, parent, false);
                    inputPostHolder = new InputPostViewHolder();
                    inputPostHolder.editText = (EditText) convertView.findViewById(R.id.post_input_edit_text);
                    inputPostHolder.postButton = (TextView) convertView.findViewById(R.id.post_input_button);
                    inputPostHolder.addPhotoBut = (TextView) convertView.findViewById(R.id.post_input_icon_add_photo);
                    //inputPostHolder.addEmoBut = (TextView) convertView.findViewById(R.id.post_input_icon_add_emo);
                    inputPostHolder.image = (ImageView) convertView.findViewById(R.id.post_input_image);

                    convertView.setTag(inputPostHolder);
                } else {
                    inputPostHolder = (InputPostViewHolder) convertView.getTag();
                }

                inputPostHolder.addPhotoBut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ImageUtils.chooseImageFromGallery(_fragment);
                    }
                });

                inputPostHolder.postButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!_isLockInput)
                            inflatePost(inputPostHolder);
                    }
                });

                if (inputPostHolder.image.getDrawable() == null)
                    inputPostHolder.image.setVisibility(View.GONE);
                else
                    inputPostHolder.image.setVisibility(View.VISIBLE);

                break;

            case TEXT:
                final TextPostViewHolder textPostHolder;

                if (convertView == null) {
                    convertView = getInflater().inflate(R.layout.list_item_post_text, parent, false);

                    textPostHolder = new TextPostViewHolder(convertView);

                    convertView.setTag(textPostHolder);
                } else {
                    textPostHolder = (TextPostViewHolder) convertView.getTag();
                }

                final PostTextItem textItem = (PostTextItem) getItem(position);

                textPostHolder.username.setText(textItem.getUserName());
                textPostHolder.text.setText(textItem.getPostText());
                textPostHolder.totalLike.setText(String.valueOf(textItem.getToltalLike()));
                textPostHolder.time.setText(textItem.getCreatedTime());
                ImageUtils.loadImageUrl(getContext(), textPostHolder.avatar, textItem.getAvaUrl());
                textPostHolder.avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToProfilePage(textItem.getFromId());
                    }
                });

                if (textItem.isLiked())
                    textPostHolder.like.setChecked(true);
                else
                    textPostHolder.like.setChecked(false);

                textPostHolder.like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkLike(textPostHolder.totalLike, textItem.getPostId());
                    }
                });

                textPostHolder.comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), CommentActivity.class);
                        intent.putExtra("postId", textItem.getPostId());
                        getContext().startActivity(intent);
                    }
                });

                textPostHolder.avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToProfilePage(textItem.getFromId());
                    }
                });
                break;

            case IMAGE:
                final ImagePostViewHolder imagePostHolder;

                if (convertView == null) {
                    convertView = getInflater().inflate(R.layout.list_item_post_image, parent,
                            false);

                    imagePostHolder = new ImagePostViewHolder(convertView);

                    convertView.setTag(imagePostHolder);
                } else {
                    imagePostHolder = (ImagePostViewHolder) convertView.getTag();
                }

                final PostImageItem imageItem = (PostImageItem) getItem(position);

                imagePostHolder.username.setText(imageItem.getUserName());
                imagePostHolder.text.setText(imageItem.getPostText());
                imagePostHolder.totalLike.setText(String.valueOf(imageItem.getToltalLike()));
                imagePostHolder.time.setText(imageItem.getCreatedTime());
                ImageUtils.loadImageUrl(getContext(), imagePostHolder.avatar, imageItem.getAvaUrl());
                ImageUtils.loadImageUrl(getContext(), imagePostHolder.image, imageItem.getImageUrl());

                imagePostHolder.avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToProfilePage(imageItem.getFromId());
                    }
                });

                if (imageItem.isLiked())
                    imagePostHolder.like.setChecked(true);
                else
                    imagePostHolder.like.setChecked(false);

                imagePostHolder.like.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkLike(imagePostHolder.totalLike, imageItem.getPostId());
                    }
                });

                imagePostHolder.comment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), CommentActivity.class);
                        intent.putExtra("postId", imageItem.getPostId());
                        getContext().startActivity(intent);
                    }
                });
                break;
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return ((BasePostItem) getItem(position)).getPostType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return BasePostItem.PostType.values().length;
    }

    private void inflatePost(final InputPostViewHolder inputHolder) {
        // Return if write nothing and has no image
        if (inputHolder.editText.getText().toString().length() <= 0 && inputHolder.image.getDrawable() == null) {
            ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.WARNING, "Please share something instead of nothing :)", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            return;
        }

        _spinnerProgress.setVisibility(View.VISIBLE);
        _isLockInput = true;

        final ImageView imageView = inputHolder.image;
        final String textContent = inputHolder.editText.getText().toString();
        String imageBase64 = ImageUtils.encodeToBase64(imageView);

        ChildData childData = _currentUser.getCurrentChild();
        Call<NewfeedPostResult> newfeedPostRequestCall = _appService.postNewfeed(new NewfeedPostRequest(childData.getClassId(), childData.getId(), _currentUser.getID(), textContent, imageBase64));
        newfeedPostRequestCall.enqueue(new Callback<NewfeedPostResult>() {
            @Override
            public void onResponse(Call<NewfeedPostResult> call, Response<NewfeedPostResult> response) {
                if (!response.isSuccessful()) {
                    try {
                        ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    _isLockInput = false;
                    _spinnerProgress.setVisibility(View.GONE);
                    return;
                }

                NewfeedPostResult result = response.body();

                if (!result.isSuccess())
                    return;

                if (imageView.getDrawable() != null) {
                    postImagePost(result.getPostId(), textContent, result.getCreatedTime(), result.getImageUrl());
                } else {
                    postTextPost(result.getPostId(), textContent, result.getCreatedTime());
                }

                _isLockInput = false;
                _spinnerProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<NewfeedPostResult> call, Throwable t) {
                _isLockInput = false;
                _spinnerProgress.setVisibility(View.GONE);
                ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private void postImagePost(int postId, String textContent, String createdTime, String imageUrl) {
        PostImageItem textItem = new PostImageItem(getCount(), postId, _currentUser.getID(), _currentUser.getUsername(), _currentUser.getAvatarUrl(), _currentUser.getCurrentChild().getRole(), imageUrl, textContent, false, 0, createdTime);
        addItem(textItem, 1);
    }

    private void postTextPost(int postId, String textContent, String createdTime) {
        PostTextItem textItem = new PostTextItem(getCount(), postId, _currentUser.getID(), _currentUser.getUsername(), _currentUser.getAvatarUrl(), _currentUser.getCurrentChild().getRole(), textContent, false, 0, createdTime);
        addItem(textItem, 1);
    }

    private void checkLike(final TextView likeTotal, final int postId) {
        Call<String> checkLikeCall = _appService.checkLike(_currentUser.getID(), postId);
        checkLikeCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, "Cannot connect to server!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    return;
                }

                likeTotal.setText(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                ToastUtils.makeTextImageToast((Activity) getContext(), ToastUtils.ToastType.ERROR, "Check your internet connection!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private void goToProfilePage(int userId) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra("userId", userId);
        getContext().startActivity(intent);
    }

    private static class TextPostViewHolder {
        public TextView time;
        public TextView username;
        public ImageView avatar;
        public TextView text;
        public ToggleButton like;
        public TextView comment;
        public TextView totalLike;

        public TextPostViewHolder(View convertView) {
            username = (TextView) convertView.findViewById(R.id.list_item_post_name);
            avatar = (ImageView) convertView.findViewById(R.id.list_item_post_profile_image);
            text = (TextView) convertView.findViewById(R.id.list_item_post_text);
            like = (ToggleButton) convertView.findViewById(R.id.list_item_post_like);
            comment = (TextView) convertView.findViewById(R.id.list_item_post_comment);
            totalLike = (TextView) convertView.findViewById(R.id.list_item_post_total_like);
            time = (TextView) convertView.findViewById(R.id.list_item_post_time);
        }
    }

    private static class ImagePostViewHolder extends TextPostViewHolder {
        public ImageView image;

        public ImagePostViewHolder(View convertView) {
            super(convertView);
            image = (ImageView) convertView.findViewById(R.id
                    .list_item_post_image);
        }
    }

    private static class InputPostViewHolder {
        public EditText editText;
        public ImageView image;
        public TextView postButton;
        public TextView addPhotoBut;
        public TextView addEmoBut;
    }
}
