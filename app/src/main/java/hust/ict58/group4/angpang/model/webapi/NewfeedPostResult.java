package hust.ict58.group4.angpang.model.webapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 21/05/2016.
 */
public class NewfeedPostResult {

    @SerializedName("post_id")
    private int _postId;

    @SerializedName("result")
    private boolean _isSuccess;

    @SerializedName("attachment")
    private String _imageLink;

    @SerializedName("created_at")
    private String _createdAt;

    public boolean isSuccess() {
        return _isSuccess;
    }

    public int getPostId() {
        return _postId;
    }

    public String getImageUrl() {
        return _imageLink;
    }

    public String getCreatedTime() {
        return _createdAt;
    }
}
