package hust.ict58.group4.angpang.model.webapi;

/**
 * Created by tuanminh on 24/04/2016.
 */
public class LoginRequest {

    private String username;
    private String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
