package hust.ict58.group4.angpang.model.webapi;

import java.util.ArrayList;

/**
 * Created by tuanminh on 01/05/2016.
 */
public class RegisterRequest {

    private String username;
    private String password;
    private String email;

    private ArrayList<RegisterChild> token_list;

    public RegisterRequest(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        token_list = new ArrayList<>();
    }

    public void registerChild(String token, String role) {
        token_list.add(new RegisterChild(token, role));
    }

    private class RegisterChild {

        private String childToken;
        private String userRole;

        public RegisterChild(String token, String role) {
            childToken = token;
            userRole = role;
        }
    }
}