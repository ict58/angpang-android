package hust.ict58.group4.angpang.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.HomeFragmentAdapter;


public class HomeTabFragment extends Fragment {

    private View view;

    public HomeTabFragment() {
        // Required empty public constructor
    }


    public static HomeTabFragment newInstance() {
        HomeTabFragment fragment = new HomeTabFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(view!=null){
            if(container !=null)
                container.removeView(view);
            return view;
        }

        view = inflater.inflate(R.layout.fragment_home_tab, container, false);
        setupTabLayout(view);
        return view;
    }

    private void setupTabLayout(View view) {
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.home_view_pager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.home_tab);
        HomeFragmentAdapter homeFragmentAdapter = new HomeFragmentAdapter
                (getChildFragmentManager());

        viewPager.setAdapter(homeFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
