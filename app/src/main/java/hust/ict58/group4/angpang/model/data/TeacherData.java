package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 20/05/2016.
 */
public class TeacherData {

    @SerializedName("teacher_id")
    private int _id;

    @SerializedName("teacher_name")
    private String _name;

    @SerializedName("teacher_phone")
    private String _phone;

    @SerializedName("email")
    private String _email;

    @SerializedName("teacher_address")
    private String _address;

    public TeacherData(int id, String name, String phone, String email, String address)
    {
        _id = id;
        _name = name;
        _phone = phone;
        _email = email;
        _address = address;
    }

    public int getId()
    {
        return _id;
    }

    public String getName()
    {
        return _name;
    }

    public String getPhoneNumber()
    {
        return _phone;
    }

    public String getEmail()
    {
        return _email;
    }

    public String getAddress()
    {
        return _address;
    }
}
