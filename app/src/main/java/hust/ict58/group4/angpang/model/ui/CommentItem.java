package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 20/04/2016.
 */
public class CommentItem extends BaseItem {

    private String _username;
    private String _avaUrl;
    private String _dateTime;
    private String _commentText;
    private String _role;
    private int _postId;
    private int _commentId;
    private int _fromId;

    public CommentItem(long id, int postId, int commentId, String username, String avaUrl, String dateTime, String comment, String role, int fromId) {
        super(id);
        _username = username;
        _avaUrl = avaUrl;
        _dateTime = dateTime;
        _commentText = comment;
        _role = role;
        _postId = postId;
        _commentId = commentId;
        _fromId = fromId;
    }

    public String getUsername() {
        return _username;
    }

    public String getAvatarUrl() {
        return _avaUrl;
    }

    public String getDatetime() {
        return _dateTime;
    }

    public String getCommentText() {
        return _commentText;
    }

    public String getUserRole() {
        return _role;
    }

    public int getPostId() {
        return _postId;
    }

    public int getCommentId() {
        return _commentId;
    }

    public int getFromId() {
        return _fromId;
    }
}
