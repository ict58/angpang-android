package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class BaseChildData {

    @SerializedName("child_name")
    private String _name;

    @SerializedName("child_avatar")
    private String _avatar;

    private String _role;

    @SerializedName("child_id")
    private int _id;

    public BaseChildData() {

    }

    public BaseChildData(int id, String name, String avatar, String role) {
        _id = id;
        _name = name;
        _avatar = avatar;
        _role = role;
    }

    public int getId() {
        return _id;
    }

    public String getAvatar() {
        return _avatar;
    }

    public String getName() {
        return _name;
    }

    public String getRole() {
        return _role;
    }
}
