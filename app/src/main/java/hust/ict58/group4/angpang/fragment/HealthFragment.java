package hust.ict58.group4.angpang.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;

import java.io.IOException;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.webapi.HealthStatusResponse;
import hust.ict58.group4.angpang.utils.ImageUtils;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private AppController _appController;
    private AppService _appService;
    private SwipeRefreshLayout _swipeRefreshLayout;

    private TextView overallHealth;
    private TextView height;
    private TextView weight;
    private TextView mood;
    private TextView temperature;
    private ChildData _currentChild;

    public HealthFragment() {
        // Required empty public constructor
    }

    public static HealthFragment newInstance() {
        HealthFragment fragment = new HealthFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            if (container != null)
                container.removeView(view);
            return view;
        }

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_health, container, false);

        _appController = AppController.getInstance();
        _appService = _appController.getAppService();

        _swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.frag_health_layout_swipe_refresh);

        if (_appController != null && _appController.getCurrentUser() != null && _appController.getCurrentUser().getCurrentChild() != null) {
            _currentChild = _appController.getCurrentUser().getCurrentChild();
        }

        CircularImageView childImage = (CircularImageView) view.findViewById(R.id.frag_health_image_child);
        TextView childName = (TextView) view.findViewById(R.id.frag_health_text_child_name);
        TextView childClass = (TextView) view.findViewById(R.id.frag_health_text_child_class);

        if (_currentChild == null)
            return view;

        ImageUtils.loadImageUrl(getContext(), childImage, _currentChild.getAvatar());
        childName.setText(_currentChild.getName());
        childClass.setText(_currentChild.getClassName());

        overallHealth = (TextView) view.findViewById(R.id.health_status_overall);
        weight = (TextView) view.findViewById(R.id.health_status_weight);
        height = (TextView) view.findViewById(R.id.health_status_height);
        mood = (TextView) view.findViewById(R.id.health_status_mood);
        temperature = (TextView) view.findViewById(R.id.health_status_temperature);

        _swipeRefreshLayout.setOnRefreshListener(this);
        _swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                fetchStatus();
            }
        });

        return view;
    }

    private void fetchStatus() {
        _swipeRefreshLayout.setRefreshing(true);

        Call<HealthStatusResponse> healthStatusCall = _appService.getHealthStatus(AppController.getInstance().getCurrentUser().getCurrentChild().getId());
        healthStatusCall.enqueue(new Callback<HealthStatusResponse>() {
            @Override
            public void onResponse(Call<HealthStatusResponse> call, Response<HealthStatusResponse> response) {
                if (!response.isSuccessful()) {
                    _swipeRefreshLayout.setRefreshing(false);
                    try {
                        ToastUtils.makeTextImageToast(getActivity(), ToastUtils.ToastType.ERROR, response.errorBody().string(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }

                HealthStatusResponse healthStatusResponse = response.body();
                overallHealth.setText(healthStatusResponse.getOverallHealth());
                weight.setText(healthStatusResponse.getWeight());
                height.setText(healthStatusResponse.getHeight());
                mood.setText(healthStatusResponse.getMood());
                temperature.setText(healthStatusResponse.getCurrentActivity());

                _swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<HealthStatusResponse> call, Throwable t) {
                _swipeRefreshLayout.setRefreshing(false);
                ToastUtils.makeTextImageToast(getActivity(), ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onRefresh() {
        fetchStatus();
    }
}
