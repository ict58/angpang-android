package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.activity.MainActivity;
import hust.ict58.group4.angpang.model.ui.AlbumImageItem;
import hust.ict58.group4.angpang.view.SquareImageView;

/**
 * Created by tuanminh on 27/04/2016.
 */
public class AlbumAdapter extends BaseListAdapter {

    public AlbumAdapter(Context context) {
        super(context);
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        final GridHolder gridHolder;

        if (convertView == null) {
            convertView = getInflater().inflate(R.layout.grid_item_album, parent, false);

            gridHolder = new GridHolder();
            gridHolder.imageView = (SquareImageView) convertView.findViewById(R.id.grid_item_album_image);
            gridHolder.textView = (TextView) convertView.findViewById(R.id.grid_item_album_description);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    getContext().startActivity(intent);
                }
            });
            convertView.setTag(gridHolder);
        } else {
            gridHolder = (GridHolder) convertView.getTag();
        }

        AlbumImageItem albumImage = (AlbumImageItem) getItem(position);
        albumImage.loadImage(getContext(), gridHolder.imageView);
        gridHolder.textView.setText(albumImage.getDescription());
        return convertView;
    }

    private static class GridHolder {
        public SquareImageView imageView;
        public TextView textView;
    }
}
