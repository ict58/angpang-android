package hust.ict58.group4.angpang.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.AlbumAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.ImageData;
import hust.ict58.group4.angpang.model.ui.AlbumImageItem;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private AlbumAdapter _albumAdapter;
    private AppService _appService;
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        Toolbar toolbar = (Toolbar) findViewById(R.id.act_album_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Album");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        _albumAdapter = new AlbumAdapter(this);

        GridView gridView = (GridView) findViewById(R.id.act_album_grid_view);
        gridView.setAdapter(_albumAdapter);

        _appService = AppController.getInstance().getAppService();

        _swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.act_album_layout_swipe_refresh);
        _swipeRefreshLayout.setOnRefreshListener(this);
        _swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                fetchAlbum();
            }
        });
    }

    private void fetchAlbum() {
        _swipeRefreshLayout.setRefreshing(true);

        Call<ArrayList<ImageData>> albumCall = _appService.getAlbumImages(AppController.getInstance().getCurrentUser().getCurrentChild().getId());
        albumCall.enqueue(new Callback<ArrayList<ImageData>>() {
            @Override
            public void onResponse(Call<ArrayList<ImageData>> call, Response<ArrayList<ImageData>> response) {
                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast(AlbumActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.login_wrong_username_password), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    _swipeRefreshLayout.setRefreshing(false);
                    return;
                }

                ArrayList<ImageData> images = response.body();

                for (int i = 0; i < images.size(); i++) {
                    ImageData imageData = images.get(i);
                    _albumAdapter.addItem(new AlbumImageItem(imageData.getId(), imageData.getUrl(), imageData.getDescription()));
                }

                _swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<ImageData>> call, Throwable t) {
                _swipeRefreshLayout.setRefreshing(false);
                ToastUtils.makeTextImageToast(AlbumActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onRefresh() {
        _albumAdapter.clearAll();
        fetchAlbum();
    }
}
