package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class ChildProfileItem extends BaseItem {

    private String _name;
    private String _avatar;
    private String _gender;
    private String _birthday;
    private String _role;

    public ChildProfileItem(long id, String name, String avaUrl, String gender, String birthday, String role) {
        super(id);
        _name = name;
        _avatar = avaUrl;
        _gender = gender;
        _birthday = birthday;
        _role = role;
    }

    public String getName() {
        return _name;
    }

    public String getAvatar() {
        return _avatar;
    }

    public String getGender() {
        return _gender;
    }

    public String getBirthday() {
        return _birthday;
    }

    public String getRole() {
        return _role;
    }
}
