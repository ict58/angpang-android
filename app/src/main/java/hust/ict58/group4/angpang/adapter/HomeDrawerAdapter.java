package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.model.ui.BaseHomeDrawerItem;
import hust.ict58.group4.angpang.model.ui.HomeDrawerChildItem;
import hust.ict58.group4.angpang.model.ui.HomeDrawerOptionItem;
import hust.ict58.group4.angpang.utils.ImageUtils;

public class HomeDrawerAdapter extends BaseListAdapter {

    public HomeDrawerAdapter(Context context) {
        super(context);
    }

    public enum ViewType {CHILD_LAYOUT, OPTION_LAYOUT}

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        ViewType itemViewType = ((BaseHomeDrawerItem) getItem(position)).getViewType();

        switch (itemViewType) {
            case CHILD_LAYOUT:

                final ChildViewHolder childViewHolder;

                if (convertView == null) {
                    convertView = getInflater().inflate(
                            R.layout.list_item_nav_child, parent,
                            false);
                    childViewHolder = new ChildViewHolder();
                    childViewHolder.dividerTop = convertView
                            .findViewById(R.id.divider_top);
                    childViewHolder.avatar = (CircularImageView) convertView.findViewById(R.id
                            .list_item_nav_child_avatar);
                    childViewHolder.name = (TextView) convertView.findViewById(R.id.list_item_nav_child_name);
                    childViewHolder.dividerBottom = convertView
                            .findViewById(R.id.divider_bottom);
                    convertView.setTag(childViewHolder);
                } else {
                    childViewHolder = (ChildViewHolder) convertView.getTag();
                }

                HomeDrawerChildItem childItem = (HomeDrawerChildItem) getItem(position);
                ImageUtils.loadImageUrl(getContext(), childViewHolder.avatar, childItem.getAvatarUrl());
                childViewHolder.name.setText(childItem.getChildName());

                if (position != 0)
                    childViewHolder.dividerTop.setVisibility(View.GONE);

                break;

            case OPTION_LAYOUT:

                final OptionViewHolder optionViewHolder;

                if (convertView == null) {
                    convertView = getInflater().inflate(
                            R.layout.list_item_nav_option, parent,
                            false);
                    optionViewHolder = new OptionViewHolder();
                    optionViewHolder.dividerTop = convertView
                            .findViewById(R.id.divider_top);
                    optionViewHolder.icon = (TextView) convertView.findViewById(R.id.list_item_nav_option_icon);
                    optionViewHolder.title = (TextView) convertView.findViewById(R.id.list_item_nav_option_title);
                    optionViewHolder.dividerBottom = convertView
                            .findViewById(R.id.divider_bottom);
                    convertView.setTag(optionViewHolder);
                } else {
                    optionViewHolder = (OptionViewHolder) convertView.getTag();
                }

                HomeDrawerOptionItem optionItem = (HomeDrawerOptionItem) getItem
                        (position);
                optionViewHolder.icon.setText(optionItem.getIcon());
                optionViewHolder.title.setText(optionItem.getOptionName());

                if (position != 0)
                    optionViewHolder.dividerTop.setVisibility(View.GONE);

                break;
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return ((BaseHomeDrawerItem) getItem(position)).getViewType().ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return ViewType.values().length;
    }


    private static class OptionViewHolder {
        public TextView icon;
        public TextView title;
        public View dividerTop;
        public View dividerBottom;
    }

    private static class ChildViewHolder {
        public CircularImageView avatar;
        public TextView name;
        public TextView role;
        public View dividerTop;
        public View dividerBottom;
    }
}
