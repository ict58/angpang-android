package hust.ict58.group4.angpang.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.HomeDrawerAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.fragment.HomeTabFragment;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.BaseHomeDrawerItem;
import hust.ict58.group4.angpang.model.ui.HomeDrawerChildItem;
import hust.ict58.group4.angpang.model.ui.HomeDrawerOptionItem;
import hust.ict58.group4.angpang.utils.ImageUtils;

public class MainActivity extends AppCompatActivity {

    private FragmentManager _fragmentManager;
    private ListView _drawerList;
    private DrawerLayout _drawerLayout;
    private ActionBarDrawerToggle _drawerToggle;

    private UserData _currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);

        _currentUser = AppController.getInstance().getCurrentUser();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_tab_social);
        setSupportActionBar(toolbar);

        if (_currentUser != null)
            setupNavigationDrawer();

        _fragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = _fragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.main_frame_layout, new HomeTabFragment()).commit();
    }

    private void setupNavigationDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_tab_social);
        setSupportActionBar(toolbar);

        ImageView avatarImageView = (ImageView) findViewById(R.id.toolbar_tab_social_image);
        ImageUtils.loadImageUrl(this, avatarImageView, _currentUser.getAvatarUrl());

        TextView nameTextView = (TextView) findViewById(R.id.toolbar_tab_social_name);
        nameTextView.setText(_currentUser.getUsername());

        TextView email = (TextView) findViewById(R.id.toolbar_tab_social_place);
        email.setText(_currentUser.getEmail());

        _drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        _drawerList = (ListView) findViewById(R.id.list_view);

        _drawerLayout.closeDrawers();
        _drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        HomeDrawerAdapter homeDrawerAdapter = new HomeDrawerAdapter(this);

        ArrayList<ChildData> childDataArrayList = _currentUser.getChildList();

        for (int i = 0; i < childDataArrayList.size(); i++) {
            final ChildData childData = childDataArrayList.get(i);
            homeDrawerAdapter.addItem(new HomeDrawerChildItem(i, childData.getAvatar(), childData.getName(), new Runnable() {
                @Override
                public void run() {
                    _currentUser.setCurrentChild(childData);
                }
            }));
        }

        homeDrawerAdapter.addItem(new HomeDrawerOptionItem(homeDrawerAdapter.getCount(), getResources().getString(R.string.material_icon_home), "Bulletin Board", new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), BulletinBoardActivity.class);
                startActivity(intent);
            }
        }));
        homeDrawerAdapter.addItem(new HomeDrawerOptionItem(homeDrawerAdapter.getCount(), getResources().getString(R.string.material_icon_album), "Album", new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), AlbumActivity.class);
                startActivity(intent);
            }
        }));

        homeDrawerAdapter.addItem(new HomeDrawerOptionItem(homeDrawerAdapter.getCount(), getResources().getString(R.string.material_icon_bookmark), "Contacts", new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), ContactActivity.class);
                startActivity(intent);
            }
        }));

        homeDrawerAdapter.addItem(new HomeDrawerOptionItem(homeDrawerAdapter.getCount(), getResources().getString(R.string.material_icon_settings), "Settings", new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(intent);
            }
        }));

        homeDrawerAdapter.addItem(new HomeDrawerOptionItem(homeDrawerAdapter.getCount(), getResources().getString(R.string.material_icon_logout), "Log out", new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }));

        _drawerList.setAdapter(homeDrawerAdapter);


        _drawerList.setOnItemClickListener(new DrawerItemClickListener());
        _drawerList.setBackgroundResource(R.drawable.splash_screen_background);
        _drawerList.getLayoutParams().width = (int) getResources()
                .getDimension(R.dimen.drawer_width_travel);

        _drawerToggle = new ActionBarDrawerToggle(this, _drawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        _drawerLayout.setDrawerListener(_drawerToggle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (_drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            BaseHomeDrawerItem drawerItem = (BaseHomeDrawerItem) parent.getItemAtPosition(position);
            drawerItem.onClick(view);
            _drawerLayout.closeDrawer(_drawerList);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        _drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        _drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Registering receiver on activity resume
    /*@Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(_broadcastReceiver,
                new IntentFilter(GCMRegistrationService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(_broadcastReceiver,
                new IntentFilter(GCMRegistrationService.REGISTRATION_ERROR));
    }


    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(_broadcastReceiver);
    }*/
}
