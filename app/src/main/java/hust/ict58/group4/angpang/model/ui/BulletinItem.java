package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class BulletinItem extends BaseItem {

    private String _title;
    private String _message;

    public BulletinItem(long id, String title, String message) {
        super(id);
        _title = title;
        _message = message;
    }

    public String getTitle() {
        return _title;
    }

    public String getMessage() {
        return _message;
    }
}
