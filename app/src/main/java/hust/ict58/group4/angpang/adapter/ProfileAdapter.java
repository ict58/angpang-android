package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.model.ui.ChildProfileItem;
import hust.ict58.group4.angpang.utils.ImageUtils;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class ProfileAdapter extends BaseListAdapter {
    public ProfileAdapter(Context context) {
        super(context);
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = getInflater().inflate(R.layout.list_item_act_profile_child_info, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.childName = (TextView) convertView.findViewById(R.id.list_item_act_profile_child_name);
            viewHolder.childAvatar = (ImageView) convertView.findViewById(R.id.list_item_act_profile_image_child);
            viewHolder.userRole = (TextView) convertView.findViewById(R.id.list_item_act_profile_role);
            viewHolder.childGender = (TextView) convertView.findViewById(R.id.list_item_act_profile_gender);
            viewHolder.childBirthday = (TextView) convertView.findViewById(R.id.list_item_act_profile_birthday);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ChildProfileItem childProfileItem = (ChildProfileItem) getItem(position);
        viewHolder.childName.setText(childProfileItem.getName());
        viewHolder.childBirthday.setText(childProfileItem.getBirthday());
        viewHolder.userRole.setText(childProfileItem.getRole());
        viewHolder.childGender.setText(childProfileItem.getGender());
        ImageUtils.loadImageUrl(getContext(), viewHolder.childAvatar, childProfileItem.getAvatar());

        return convertView;
    }

    private static class ViewHolder {
        public TextView childName;
        public ImageView childAvatar;
        public TextView userRole;
        public TextView childGender;
        public TextView childBirthday;
    }
}
