package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 01/05/2016.
 */
public class PostInputItem extends BasePostItem {
    public PostInputItem(long id, int postId, int fromId, String username, String avaUrl, String role, String text, boolean isLiked, int totalLike, String createAt) {
        super(id, postId, fromId, username, avaUrl, role, text, isLiked, totalLike, createAt);
    }

    @Override
    protected PostType setPostType() {
        return PostType.INPUT;
    }
}
