package hust.ict58.group4.angpang.activity;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.utils.ToastUtils;

public class VideoActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

    private VideoView emVideoView;
    private String _url;
    private ProgressDialog _dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        // Get ip from cctv camera
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                ToastUtils.makeTextImageToast(VideoActivity.this, ToastUtils.ToastType.ERROR, "Camera in maintaining progress!", Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                return;
            } else {
                _url = extras.getString("url");
            }
        } else {
            _url = (String) savedInstanceState.getSerializable("url");
        }

        _dialog = new ProgressDialog(this);
        _dialog.setMessage("Buffering...");

        emVideoView = (VideoView) findViewById(R.id.act_video_view);
        emVideoView.setOnPreparedListener(this);
        emVideoView.setZOrderOnTop(true);

        try {
            _dialog.show();

            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    VideoActivity.this);
            mediacontroller.setAnchorView(emVideoView);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(_url);
            emVideoView.setMediaController(mediacontroller);
            emVideoView.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            _dialog.hide();
            e.printStackTrace();
        }
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        _dialog.hide();
        //Starts the video playback as soon as it is ready
        emVideoView.start();
    }

}
