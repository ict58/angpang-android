package hust.ict58.group4.angpang.model.ui;

import android.content.Context;
import android.widget.ImageView;

import hust.ict58.group4.angpang.utils.ImageUtils;

/**
 * Created by tuanminh on 27/04/2016.
 */
public class BaseImage extends BaseItem {

    private String _url;

    public BaseImage(long id, String url) {
        super(id);
        _url = url;
    }

    public String getUrl() {
        return _url;
    }

    public void loadImage(Context context, ImageView imageView) {
        ImageUtils.loadImageUrl(context, imageView, _url);
    }
}
