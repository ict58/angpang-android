package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 24/04/2016.
 */
public class Camera extends BaseItem {

    private String _description;
    private String _ip;

    public Camera(long id, String description, String ip) {
        super(id);
        _description = description;
    }

    public String getDescription()
    {
        return _description;
    }

    public String getStreammingUrl()
    {
        return _ip;
    }
}
