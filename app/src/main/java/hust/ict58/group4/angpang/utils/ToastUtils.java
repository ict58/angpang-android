package hust.ict58.group4.angpang.utils;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hust.ict58.group4.angpang.R;

/**
 * Created by tuanminh on 25/04/2016.
 */
public class ToastUtils {

    public enum ToastType {
        ERROR,
        WARNING,
        OK
    }

    public static Toast makeTextImageToast(Activity context, ToastType toastType, String text, int gravity, int duration) {
        Toast toast = new Toast(context);
        View toastView = context.getLayoutInflater().inflate(R.layout.toast_error, null, false);

        TextView textView = (TextView) toastView.findViewById(R.id.toast_error_text);
        ImageView imageView = (ImageView) toastView.findViewById(R.id.toast_error_image);

        textView.setText(text);

        if (toastType == ToastType.ERROR) {
            toastView.setBackground(context.getResources().getDrawable(R.drawable.toast_error_border));
            imageView.setBackground(context.getResources().getDrawable(R.drawable.toast_error_red));
        } else if (toastType == ToastType.WARNING) {
            toastView.setBackground(context.getResources().getDrawable(R.drawable.toast_warning_border));
            imageView.setBackground(context.getResources().getDrawable(R.drawable.toast_warning_yellow));
        } else if (toastType == ToastType.OK) {
            toastView.setBackground(context.getResources().getDrawable(R.drawable.toast_ok_border));
            imageView.setBackground(context.getResources().getDrawable(R.drawable.toast_ok_green_check));
        }

        toast.setView(toastView);
        toast.setGravity(gravity, 0, 0);
        toast.setDuration(duration);
        toast.show();
        return toast;
    }
}
