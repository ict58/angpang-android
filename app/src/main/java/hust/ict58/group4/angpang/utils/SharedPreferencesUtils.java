package hust.ict58.group4.angpang.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by khanhhung on 5/16/16.
 */
public class SharedPreferencesUtils {

    private static final SharedPreferencesUtils _instance = new SharedPreferencesUtils();

    public static final String PREFS_NAME = "AngPangPref";
    private static final String PREF_USERNAME = "username";
    private static final String PREF_PASSWORD = "password";
    private static final String PREF_IS_REMEMBER_ME = "isRememberMe";
    public static final String PREF_SETTING_RECEIVE_NOTI = "setting_receive_noti";
    private static final String PREF_SETTING_VIBRATION = "setting_vibration";
    private static final String PREF_SETTING_SOUND = "setting_sound";

    private SharedPreferences _pref;

    private SharedPreferencesUtils() {

    }

    public void setContext(Context context) {
        _pref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void rememberAccount(String username, String password) {
        _pref.edit().putString(PREF_USERNAME, username).putString(PREF_PASSWORD, password).putBoolean(PREF_IS_REMEMBER_ME, true).commit();
    }

    public void uncheckRememberMe() {
        _pref.edit().putBoolean(PREF_IS_REMEMBER_ME, false).commit();
    }

    public void checkboxReceiveNoti(boolean isChecked) {
        _pref.edit().putBoolean(PREF_SETTING_RECEIVE_NOTI, isChecked).commit();
    }

    public void checkboxVibration(boolean isChecked) {
        _pref.edit().putBoolean(PREF_SETTING_VIBRATION, isChecked).commit();
    }

    public void checkboxSound(boolean isChecked) {
        _pref.edit().putBoolean(PREF_SETTING_SOUND, isChecked).commit();
    }

    public boolean isRememberMeChecked() {
        return _pref.getBoolean(PREF_IS_REMEMBER_ME, false);
    }

    public String getRememberedUsername() {
        return _pref.getString(PREF_USERNAME, "anonymous");
    }

    public String getRememberedPassword() {
        return _pref.getString(PREF_PASSWORD, "");
    }

    public boolean isEnableRecevieNoti() {
        return _pref.getBoolean(PREF_SETTING_RECEIVE_NOTI, true);
    }

    public boolean isEnableVibration() {
        return _pref.getBoolean(PREF_SETTING_VIBRATION, true);
    }

    public boolean isEnableSound() {
        return _pref.getBoolean(PREF_SETTING_SOUND, true);
    }

    public static synchronized SharedPreferencesUtils getInstance() {
        return _instance;
    }
}
