package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 22/05/2016.
 */
public class ProfileChildData extends BaseChildData {

    @SerializedName("gender")
    private int _gender;

    @SerializedName("dob")
    private String _birthday;

    public String getGender() {
        if (_gender == 0)
            return "Female";
        return "Male";
    }

    public String getBirthday() {
        return _birthday;
    }
}
