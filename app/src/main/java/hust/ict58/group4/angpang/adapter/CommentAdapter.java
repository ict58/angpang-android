package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.activity.ProfileActivity;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.model.data.UserData;
import hust.ict58.group4.angpang.model.ui.CommentItem;
import hust.ict58.group4.angpang.utils.ImageUtils;

/**
 * Created by tuanminh on 17/05/2016.
 */
public class CommentAdapter extends BaseListAdapter {

    private UserData _currentUser;

    public CommentAdapter(Context context) {
        super(context);
        _currentUser = AppController.getInstance().getCurrentUser();
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {
        CommentViewHolder viewHolder;

        if (convertView == null) {
            convertView = getInflater().inflate(R.layout.post_comment, parent, false);
            viewHolder = new CommentViewHolder();

            viewHolder.username = (TextView) convertView.findViewById(R.id.list_item_comment_username);
            viewHolder.date = (TextView) convertView.findViewById(R.id.list_item_comment_time);
            viewHolder.comment = (TextView) convertView.findViewById(R.id.list_item_comment_text);
            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.list_item_comment_profile_image);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CommentViewHolder) convertView.getTag();
        }

        final CommentItem item = (CommentItem) getItem(position);

        viewHolder.username.setText(item.getUsername());
        viewHolder.comment.setText(item.getCommentText());
        viewHolder.date.setText(item.getDatetime());

        ImageUtils.loadImageUrl(getContext(), viewHolder.avatar, item.getAvatarUrl());

        viewHolder.avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToProfilePage(item.getFromId());
            }
        });

        return convertView;
    }

    private void goToProfilePage(int userId) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra("userId", userId);
        getContext().startActivity(intent);
    }

    private static class CommentViewHolder {
        public TextView username;
        public TextView date;
        public TextView comment;
        public ImageView avatar;
    }
}
