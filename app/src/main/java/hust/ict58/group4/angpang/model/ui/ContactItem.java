package hust.ict58.group4.angpang.model.ui;

/**
 * Created by tuanminh on 20/05/2016.
 */
public class ContactItem extends BaseItem {

    public String _name;
    public String _phone;
    public String _email;
    public String _address;

    public ContactItem(long id, String name, String phone, String email, String address) {
        super(id);
        _name = name;
        _phone = phone;
        _email = email;
        _address = address;
    }

    public String getName()
    {
        return _name;
    }

    public String getPhone()
    {
        return _phone;
    }

    public String getEmail()
    {
        return _email;
    }

    public String getAddress()
    {
        return _address;
    }
}
