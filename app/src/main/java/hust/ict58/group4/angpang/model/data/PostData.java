package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khanhhung on 5/20/16.
 */
public class PostData {

    @SerializedName("post_id")
    private int _postId;

    @SerializedName("from_id")
    private int _fromId;

    @SerializedName("from_user")
    private String _fromUsername;

    @SerializedName("from_ava")
    private String _fromAvatar;

    @SerializedName("post_content")
    private String _postContent;

    @SerializedName("likes")
    private int _likeTotal;

    @SerializedName("attachment")
    private String _attachment;

    @SerializedName("is_liked")
    private boolean _isLiked;

    @SerializedName("created_at")
    private String _createAt;

    public int getPostId() {
        return _postId;
    }

    public int getFromId() {
        return _fromId;
    }

    public String getFromUsername() {
        return _fromUsername;
    }

    public String getFromAvatar() {
        return _fromAvatar;
    }

    public String getPostContent() {
        return _postContent;
    }

    public String getImageAttachment() {
        return _attachment;
    }

    public int getTotalLike() {
        return _likeTotal;
    }

    public boolean isLiked() {
        return _isLiked;
    }

    public String getCreatedTime() {
        return _createAt;
    }
}
