package hust.ict58.group4.angpang.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.activity.VideoActivity;
import hust.ict58.group4.angpang.model.ui.Camera;

/**
 * Created by tuanminh on 24/04/2016.
 */
public class CCTVAdapter extends BaseListAdapter {

    public CCTVAdapter(Context context) {
        super(context);
    }

    @Override
    public View getViewData(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = getInflater().inflate(R.layout.list_item_cctv, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.description = (TextView) convertView.findViewById(R.id.list_item_cctv_description);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Camera camera = (Camera) getItem(position);
        viewHolder.description.setText(camera.getDescription());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), VideoActivity.class);
                intent.putExtra("url", camera.getStreammingUrl());
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        public TextView description;
    }
}
