package hust.ict58.group4.angpang.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hust.ict58.group4.angpang.R;

public class ContactTabFragment extends Fragment {

    private View view;

    public ContactTabFragment() {
        // Required empty public constructor
    }

    public static ContactTabFragment newInstance() {
        ContactTabFragment fragment = new ContactTabFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            if (container != null)
                container.removeView(view);
            return view;
        }
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact_tab, container, false);
        return view;
    }
}
