package hust.ict58.group4.angpang.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by tuanminh on 19/05/2016.
 */
public class GCMTokenRefreshService extends InstanceIDListenerService {

    //If the token is changed registering the device again
    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, GCMRegistrationService.class);
        startService(intent);
    }
}
