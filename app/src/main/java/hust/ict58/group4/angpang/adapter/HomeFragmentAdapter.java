package hust.ict58.group4.angpang.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import hust.ict58.group4.angpang.fragment.CCTVFragment;
import hust.ict58.group4.angpang.fragment.HealthFragment;
import hust.ict58.group4.angpang.fragment.NewFeedFragment;
import hust.ict58.group4.angpang.fragment.TimeTableFragment;

/**
 * Created by tuanminh on 27/03/2016.
 */
public class HomeFragmentAdapter extends FragmentPagerAdapter {

    private String[] _tabTitles = {"Health", "New Feeds", "CCTV", "Time Table"};

    public HomeFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return HealthFragment.newInstance();
        else if (position == 1)
            return NewFeedFragment.newInstance();
        else if (position == 2)
            return CCTVFragment.newInstance();

        return TimeTableFragment.newInstance();
    }

    @Override
    public int getCount() {
        return _tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return _tabTitles[position];
    }
}
