package hust.ict58.group4.angpang.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import hust.ict58.group4.angpang.R;
import hust.ict58.group4.angpang.adapter.ContactAdapter;
import hust.ict58.group4.angpang.controller.AppController;
import hust.ict58.group4.angpang.controller.AppService;
import hust.ict58.group4.angpang.model.data.ChildData;
import hust.ict58.group4.angpang.model.data.TeacherData;
import hust.ict58.group4.angpang.model.ui.ContactItem;
import hust.ict58.group4.angpang.utils.ToastUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactActivity extends AppCompatActivity {

    private AppController _appController;
    private AppService _appService;
    private ChildData _currentChild;
    private ContactAdapter _contactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Contacts");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        _contactAdapter = new ContactAdapter(this);

        ListView listView = (ListView) findViewById(R.id.act_contact_list_view);
        listView.setAdapter(_contactAdapter);

        _appController = AppController.getInstance();
        _currentChild = _appController.getCurrentUser().getCurrentChild();
        _appService = _appController.getAppService();

        if (_currentChild.hasLoadedContactList()) {
            loadContactToAdapter();
        } else {
            fetchContact();
        }
    }

    private void fetchContact() {
        Call<ArrayList<TeacherData>> teacherDataCall = _appService.getTeacherData(_currentChild.getClassId());
        teacherDataCall.enqueue(new Callback<ArrayList<TeacherData>>() {
            @Override
            public void onResponse(Call<ArrayList<TeacherData>> call, Response<ArrayList<TeacherData>> response) {
                if (!response.isSuccessful()) {
                    ToastUtils.makeTextImageToast(ContactActivity.this, ToastUtils.ToastType.ERROR, getString(R.string.response_not_successful), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
                    return;
                }

                _currentChild.addContactList(response.body());
                loadContactToAdapter();
            }

            @Override
            public void onFailure(Call<ArrayList<TeacherData>> call, Throwable t) {
                ToastUtils.makeTextImageToast(ContactActivity.this, ToastUtils.ToastType.ERROR, t.toString(), Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
            }
        });
    }

    private void loadContactToAdapter() {
        for (ContactItem contactItem : _currentChild.getContactList()) {
            _contactAdapter.addItem(contactItem);
        }
    }

}
