package hust.ict58.group4.angpang.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tuanminh on 21/05/2016.
 */
public class FoodMenuData {

    private static final String DAY_NAMES[] = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};

    @SerializedName("date")
    private String _date;

    @SerializedName("food_menu")
    private String _foodMenu;

    public String getDate() {
        return _date;
    }

    public String getFoodMenu() {
        return _foodMenu;
    }

    public boolean compareToCalendarDay(int calendarDay) {
        if (_date.compareTo(String.valueOf(getDayNameByIndex(calendarDay))) == 0)
            return true;

        return false;
    }

    private String getDayNameByIndex(int index) {
        return DAY_NAMES[index - 1];
    }
}
